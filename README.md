ReadMe
================

- [Introduction](#introduction)
- [Prerequisite](#prerequisite)
- [Generating the docker image](#generating-the-docker-image)
- [Basic Usage](#basic-usage)
- [Detail documentation](#detail-documentation)
  - [Required scripts](#required-scripts)
  - [File inputs](#file-inputs)
    - [Protein related](#protein-related)
    - [Phenotype related](#phenotype-related)
    - [Risk factors related](#risk-factors-related)
    - [GRS related](#grs-related)
  - [Data handling](#data-handling)
  - [Model Training](#model-training)
  - [Misc](#misc)
- [Running the pipeline](#running-the-pipeline)
- [Expected output](#expected-output)

<!-- README.md is generated from README.Rmd. Please edit that file -->

# Introduction

This repository contain the master script for the UK biobank Pharma
Proteomics Project analyses script.

# Prerequisite

For this script to work, we will need to install `docker` and `miniwdl`

First, you will need to install `docker`:

    sudo apt-get remove docker docker-engine docker.io containerd runc

You will then need to setup user group. Instructions can be found
[here](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

Assuming you have python 3.6 or higher install in your system, you can
install `miniwdl` with :

    pip3 install miniwdl

# Generating the docker image

In the base directory, run Run:

    docker build -t ukb_ppp -f ukb_ppp.docker .

And you should have the docker image `ukb_ppp`.

# Basic Usage

You will need the following files to use our pipeline:

1.  UK Biobank Protein expression data in the wide format (e.g. one row
    per sample, one protein per column). We assume the sample ID column
    in this file should either be `RGCpID` or `IID` (If both are
    presented, will use `RGCpID`). Any columns with format `*_OID*` will
    be assumed to be protein expression.

2.  Phenotype file. We currently support binary phenotype and also
    longitudinal data. Longitudinal data (e.g. incidence cases). For
    longitudinal data, it must contain both the incident case
    information and follow up time period

The following files are optional:

1.  UK Biobank Protein covariate file. This file should contain
    covariates that will only include in a model if protein expression
    is included. This file must contain the same sample ID column as the
    protein expression file.
2.  Predictor file. This file should contain all predictors
    e.g. clinical scores, risk factors, covariates etc. If not provided,
    will assume those information are included in the phenotype file.
    This file should have a sample ID column that is identical to either
    the protein expression file or the phenotype file.
3.  GRS file. This file should contain the GRS. We will only include GRS
    in our models if this is provided.

All parameters and their detail documentation can be found in the
following section:

# Detail documentation

## Required scripts

These scripts should all be accessible under the `r` directory. You can
replace those script with customized script providing that they generate
the expected output file.

1.  **preparation_script**: `1_prepare-sample-input.R`, this script is
    responsible for harmonizing all input files

2.  **protein_selection_script**: `2_cfs-protein-selection.R`, This is
    the script responsible for performing the protein selection. This is
    by far the most time consuming step of the whole pipeline.

3.  **coefficient_script**: `3_protein-model-training.R`, This is the
    script responsible for calculating the coefficient for the combined
    protein score and with other variables included.

4.  **general_training_script**: `4_non-protein-model-training.R`,
    similar to 3, but for non-protein model.

5.  **prediction_script**: `5_calculate-predicted-scores.R`, given model
    RDS files, this script will generate the prediction score for each
    samples in the validated data set. This will generate a long format
    file, with sample ID, Predicted score, the prediction model and the
    phenotype of interest.

6.  **predict_summary_script (optional)**:
    `6_model-performance-for-perm.R`, given the prediction scores,
    generate the model statistics.

## File inputs

### Protein related

1.  **protein_expression**: UK Biobank PPP expression data. Must contain
    sample ID in either the `RGCpID` or `IID` column. All column with
    `*_OID*` will be assumed to be protein. If `sample_grouping` is not
    provided and `rand_split` is null, there should be a `Discovery`
    column containing the grouping information.

2.  **protein_covariate_file**: File containing covariates that should
    only be used when protein expressions are included in the model.
    Must contain the same sample ID as the protein expression file. If
    `protein_covariates=null`, will include all column in this file as
    protein covariates.

3.  **protein_covariates**: Columns found in the
    `protein_covariate_file` that will **only** be used when protein
    expressions are included in the model. **These will not be used for
    prediction**

### Phenotype related

1.  **phenotype**: Phenotype file. Should contain all phenotype
    information. If `sample_map=null`, we will assume sample ID in the
    `pheno_id` column can be mapped to the sample ID from the
    `protein_expression` file, otherwise, we will use the `sample_map`
    file to help with ID mapping. If `predictors=null`, we will assume
    all risk factors and covariates can also be found in the phenotype
    file.
2.  **pheno_id**: the sample ID of the phenotype file. This is required.
3.  **pheno_train**: The column name of the phenotype that should be
    used for training
4.  **pheno_valid**: The column name of the phenotype that should be
    used for validation. If not provided, will assume to be identical to
    training
5.  **time_to_event_train**: The column name of the time to event
    information that should be used for training. If not provided,
    assume to be `FlUp`. Only require if `train_valid` is `cox` or
    censoring is required.
6.  **time_to_event_valid**: The column name of the time to event
    information that should be used for validation. If not provided,
    assume to be `FlUp`. Only require if `valid_family` is `cox` or
    censoring is required.
7.  **train_family**: Data type of the training phenotype. This is for
    protein selection and model training. Can be `cox` for longitudinal
    data, `logistic`, `binomial` or `binary` for binary trait; and
    `gaussian` or `continuous` for quantitative trait.
8.  **valid_family**: Data type of the validation phenotype. This is for
    calculating the performance metrics. Can be `cox` for longitudinal
    data, `logistic`, `binomial` or `binary` for binary trait; and
    `gaussian` or `continuous` for quantitative trait.
9.  **filter_train**: Additional filtering on the training samples.
    Input format is `Column:Value`. For example, with `PRV_FL:1`, any
    samples with `PRV_FL == 1` will be removed from the training
    dataset. Multiple filtering is allowed using comma separated list.
    Filtering is done before transformation unless
    `trans_before_merge=true`.
10. **filter_valid**: Additional filtering on the validation samples.
    Input format is `Column:Value`. For example, with `PRV_FL:1`, any
    samples with `PRV_FL == 1` will be removed from the validation
    dataset. Multiple filtering is allowed using comma separated list.
    Filtering is done before transformation unless
    `trans_before_merge=true`.

### Risk factors related

1.  **predictors**: A file containing all risk factors and covariates.
    This file should have a sample ID column that is identical to either
    the protein expression file or the phenotype file. If not provided,
    will assume all risk factors and covariates are included in the
    phenotype file.
2.  **covariates**: Covariates that should be used when training the
    model. **Will not be used for prediction**. This should be a comma
    separated list of column names. Can contain interaction and power
    terms, which should be automatically taken care of by the script.
3.  **baselines**: Covariates that will be included in all models. Will
    also be used for prediction
4.  **clinical_scores**: Column contain established clinical scores.
    This variable will not undergo any transformation and will used as
    is in the model (i.e. assume coeffcient of 1)
5.  **clinical_variables**: Columns containing relevant clinical
    variables. These variables will be transformed. Individual
    coefficient will be fitted using the training data and applied to
    the validation dataset.

### GRS related

1.  **grs**: The GRS file. Should contain at least two columns: The
    sample ID column (defined using `grs_id`) and the score column
    (defined using `grs_score`). When not provided, GRS will not be
    included in the model
2.  **grs_id**: Column in the grs file that represent the sample ID.
    Assume this will match to the sample ID of the protein expression
    data.
3.  **grs_score**: Column in the grs file that contain the GR
4.  **grs_trans**: Transformation to be performed on the GRS. Valid
    options are `rint`, `scale` and `none`.

## Data handling

1.  **na2zero**: Boolean indicate if we should treat any NA binary
    variables as 0 (no event).
2.  **impute**: Boolean indicate if we should impute missing continuous
    variables using population mean
3.  **trans_covar**: Boolean indicate if we should transform variable
    specified by `covariates`. Default is false.
4.  **continuous_trans**: Transformation to be performed on all
    continuous variables (except covariates unless `trans_covar` = true,
    and except GRS). Valid options are: `rint`, `scale` and `none`. This
    transformation will also be applied to the Protein Score.
5.  **binary_trans**: Transformation to be performed on all binary
    variables (except covariates, unless `trans_covar` = true). Valid
    options are `rint`, `scale` and `none`
6.  **trans_before_merge**: Most of our transformation is
    data-sensitive. This boolean indicate if transformation should be
    performed before or after merging all data. If false, we will
    perform transformation of the variables as each file is read, e.g.
    transform GRS across all samples with GRS scores even if only half
    of our GRS samples has protein expression data. For more complex
    handling, it is best for you to provided a transformed input instead
    of relying on the transformation procedure of the pipeline.
7.  **trans_within_split**: Should transformation be performed within
    each sample split. IF true, transformation will be performed within
    each split independently.

## Model Training

1.  **sample_grouping**: A zip file contain a collection of files, each
    with two columns. First column should be the `RGCpID` (or name that
    matched to the protein expression file) and the second column should
    be a vector of true and false, where `true` = samples that should be
    used for training
2.  **rand_split**: Proportion of samples that should be used as
    training samples. For example, 0.7 means 70% of the samples should
    be used as training. When performing random split, we will try to
    maximize sample size by taking into account of missing data.
3.  **cv_fold**: Number of cv fold used for glmnet training
4.  **censor_train**: year to right censor to in training data
5.  **censor_valid**: year to right censor to in validation data
6.  **min_censor_train**: year to left censor to in training data
7.  **min_censor_valid**: year to left censor to in validation data
8.  **remove_left_censor_train**: Whether we should remove left censored
    training data, or simply set the incident case to 0.
9.  **remove_left_censor_valid**: Whether we should remove left censored
    validate data, or simply set the incident case to 0.
10. **quantile**: Comma separated list of Top x quantiles to be
    determined as extreme
11. **absolute_risk**: Comma separated list of absolute risk thresholds
12. **protein_coef_source**: By default, for the consensus protein
    score, we use the protein coefficients obtained from the protein
    only model. A different model can be selected using this parameter.
    For example, `Clinical+GRS+Proteins` will correspond to the model
    using the coefficient with baseline, clinical risk factors, GRS and
    protein expression.

## Misc

1.  **thread**: Number of thread used for glmnet model training
2.  **seed**: Random seed used for any randomized operation
3.  **prefix**: Output prefix
4.  **sensitivity**: Sensitivity analyses by extending the exclusion
    period
5.  **probability**: If true, transform scores into probability.
6.  **docker_image**: String indicating the docker image. If instruction
    was followed, should be `ukb_ppp`.

# Running the pipeline

An example run will be as follow (replace `${script}` and all variables
with `<>`)

``` bash
miniwdl run --verbose \
    ${script}/ppp_pipeline_perm.wdl \
    preparation_script=${script}/r/1_prepare-sample-input.R \
    protein_selection_script=${script}/r/2_cfs-protein-selection.R \
    coefficient_script=${script}/r/3_protein-model-training.R \
    general_training_script=${script}/r/4_non-protein-model-training.R \
    prediction_script=${script}/r/5_calculate-predicted-scores.R \
    predict_summary_script=${script}/r/6_model-performance-form REAr-perm.R \
    protein_expression=<Protein expression> \
    phenotype=<Phenotype data> \
    sample_grouping=<Split zip file> \
    baselines="Age,Sex,Age^2,Age:Sex" \
    covariates="PC1,PC2,PC3,PC4,PC5,PC6,PC7,PC8,PC9,PC10,genotyping_array" \
    clinical_variables="BMI,CurrentSmoke" \
    na2zero=true \
    impute=true \
    continuous_trans="scale" \
    binary_trans="none" \
    trans_covar=false \
    trans_before_merge=false \
    train_family="cox" \
    time_to_event_train="FlUp" \
    valid_family="cox" \
    time_to_event_valid="FlUp" \
    seed=12345 \
    thread=8 \
    censor_train=10 \
    censor_valid=10 \
    quantile="5,10" \
    probability=true \
    sensitivity=1,2,3 \
    trans_within_split=true
```

For more options, please refer to the document within the
`ppp_pipeline_perm.wdl` script

# Expected output

All pipeline output can be found in the `_LAST/out` folder and should be
organized as follow:
