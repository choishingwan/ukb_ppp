version 1.1

struct ProteinModel{
    String parameter
    String protein_type
    File? protein_model
}

task generate_forest_plots{
    input{
        File forest_plot_script
        ProteinModel non_protein_prediction
        ProteinModel protein_predictions
        ProteinModel non_protein_risk
        ProteinModel protein_risks
        String docker_image
        String pheno
        String valid_family
        String prefix = "PGx"
        File? protein_cov
        String? covariates
        String? baselines
        String? subgroup
        Int seed = 12345
    }
    output{
        File model_output = "${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}.res"
        Array[File] figures = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*forest.png")
        Array[File] auc = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*auc.png")
    }
    Int mem_space = ceil(size(non_protein_prediction.protein_model, "GB") + 
        size(protein_predictions.protein_model, "GB")) + 5
    command <<<
        Rscript ~{forest_plot_script} \
            --input ~{non_protein_prediction.protein_model},~{protein_predictions.protein_model} \
            --risk ~{non_protein_risk.protein_model},~{protein_risks.protein_model} \
            --pheno ~{pheno} \
            --prefix ~{prefix}-~{protein_predictions.parameter}-~{protein_predictions.protein_type} \
            --family ~{valid_family} \
            --seed ~{seed} \
            ~{if defined(baselines) then "--baseline ${baselines}" else ""} \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""} \
            ~{if defined(protein_cov) then "--protein-cov ${protein_cov}" else ""} \
            ~{if defined(subgroup) then "--subgroup ${subgroup}" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}


task generate_longitudinal_plots{
    input{
        File longitudinal_plot_script
        ProteinModel non_protein_prediction
        ProteinModel protein_predictions
        ProteinModel non_protein_risk
        ProteinModel protein_risks
        String docker_image
        String pheno
        String prefix = "PGx"
        File? protein_cov
        String? covariates
        String? time_valid
        Int seed = 12345
        Boolean run_time_dependent = true
    }
    output{
        Array[File] reclassifications = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*.csv")
        Array[File] longitudinal = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*surv.png")
        Array[File] classification = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*class.png")
        Array[File] hr = glob("${prefix}-${protein_predictions.parameter}-${protein_predictions.protein_type}*hr.png")
    }
    Int mem_space = ceil(size(non_protein_prediction.protein_model, "GB") + 
        size(protein_predictions.protein_model, "GB")) + 20
    command <<<
        Rscript ~{longitudinal_plot_script} \
            --input ~{non_protein_prediction.protein_model},~{protein_predictions.protein_model} \
            --risk ~{non_protein_risk.protein_model},~{protein_risks.protein_model} \
            --prefix ~{prefix}-~{protein_predictions.parameter}-~{protein_predictions.protein_type} \
            --seed ~{seed} \
            --pheno ~{pheno} \
            ~{if defined(time_valid) then "--time ${time_valid}" else ""} \
            ~{if run_time_dependent then "--time-dependent" else "" } \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""} \
            ~{if defined(protein_cov) then "--protein-cov ${protein_cov}" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}

task combine_files{
    input{
        Array[File?] input_files
        String name
        String docker_image
    }
    output{
        File? dat = "${name}.zip"
    }
    String run = if length(input_files) != 0 then "true" else "false"
    command <<<
        if ~{run}; then
            zip -j -r ~{name}.zip ~{sep(' ', select_all(input_files))}
        fi
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "5 GB"
    }
}
