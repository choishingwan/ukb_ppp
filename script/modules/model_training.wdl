version 1.1

struct ProteinModel{
    String parameter
    String protein_type
    File? protein_model
}
struct Model{
    String name
    Array[String]+ independent
}
task non_protein_model_training{
    input{
        File general_training_script
        File training_dat
        String pheno_train
        String train_family
        String? time_to_event_train
        String? covariates
        String? baselines
        String? clinical_scores
        String? clinical_variables
        String prefix = "PGx"
        String docker_image
    }
    output{
        File non_protein_model = "${prefix}-non-protein-models.RDS"
    }
    Int mem_space = ceil(size(training_dat, "GB")) + 5
    command <<< 
        Rscript ~{general_training_script} \
            --input ~{training_dat} \
            --pheno ~{pheno_train} \
            --prefix ~{prefix} \
            --family ~{train_family} \
            ~{if defined(time_to_event_train) then "--time ${time_to_event_train}" else ""} \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""} \
            ~{if defined(baselines) then "--baseline ${baselines}" else ""} \
            ~{if defined(clinical_scores) then "--score ${clinical_scores}" else ""} \
            ~{if defined(clinical_variables) then "--clinical ${clinical_variables}" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}

task protein_selection{
    input{
        File protein_selection_script
        Model cur_model
        File training_dat
        File protein_names
        File? protein_cov
        String pheno_train
        String train_family
        String prefix="PGx"
        Int cv_fold = 5
        Int seed = 12345
        Int thread = 1
        String? time_train
        String? covariates
        String docker_image
    }
    output{
        File? coef= "${prefix}.protein-coef"
    }
    Int mem_space = ceil(size(training_dat, "GB")) + 10
    command <<< 
        Rscript ~{protein_selection_script} \
            --input ~{training_dat} \
            --protein ~{protein_names} \
            --model ~{write_json(cur_model)} \
            --pheno ~{pheno_train} \
            --family ~{train_family} \
            --cv ~{cv_fold} \
            --seed ~{seed} \
            --prefix ~{prefix} \
            --thread ~{thread} \
            ~{if defined(time_train) then "--time ${time_train}" else ""} \
            ~{if defined(protein_cov) then "--protein-cov ${protein_cov}" else ""} \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""}  
    >>>
    runtime{
        container: docker_image
        cpu: thread
        memory: "~{mem_space} GB"
    }
}

task train_protein_score_coefficients{
    input{
        Array[File] protein_models_coef
        File coefficient_script
        File training_dat
        String pheno_train
        String train_family
        String parameter
        String protein_type
        String protein_coef_source = "Proteins"
        String prefix="PGx"
        Int num_models
        File? protein_cov
        String? transform
        String? time_train
        String? covariates
        String? baselines
        String? clinical_scores
        String? clinical_variables
        String docker_image
    }
    output{
       File? model = "${prefix}-${parameter}-${protein_type}-models.RDS"
       File? figure= "${prefix}-${parameter}-${protein_type}-proteins.png"
       File? coef = "${prefix}-${parameter}-${protein_type}-protein-coefficients.csv"
       File? lrt = "${prefix}-${parameter}-${protein_type}-lrt-result.csv"
       File? fh_lrt = "${prefix}-${parameter}-${protein_type}-fh-lrt-result.csv"
       Array[File] figures = glob("${prefix}-${parameter}-${protein_type}*proteins.png")
    }
    Int mem_space =  ceil(size(training_dat, "GB")) + 5
    command <<<
        Rscript ~{coefficient_script} \
            --input ~{training_dat} \
            --num-models ~{num_models} \
            --pheno ~{pheno_train} \
            --prefix ~{prefix}-~{parameter}-~{protein_type} \
            --parameter ~{parameter} \
            --type ~{protein_type} \
            --coef ~{sep(',', protein_models_coef)} \
            --source ~{protein_coef_source} \
            --family ~{train_family} \
            ~{if defined(time_train) then "--time ${time_train}" else ""} \
            ~{if defined(protein_cov) then "--protein-cov ${protein_cov}" else ""} \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""} \
            ~{if defined(baselines) then "--baseline ${baselines}" else ""} \
            ~{if defined(clinical_scores) then "--score ${clinical_scores}" else ""} \
            ~{if defined(transform) then "--transform ${transform}" else "" } \
            ~{if defined(clinical_variables) then "--clinical ${clinical_variables}" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}


task generate_predictions{
    input{
        File prediction_script
        File validate_dat
        String pheno
        String train_family
        String prefix = "PGx"
        ProteinModel models
        String? risk_year
        String? transform
        String? quantile
        String? time_valid
        Int? top
        Boolean probability = false
        String docker_image
    }
    output{
        File? predicted = "${prefix}-${models.parameter}-${models.protein_type}.predicted.gz"
        File? within_predict = "${prefix}-${models.parameter}-${models.protein_type}.within-predicted.gz"
        Array[File] figures = glob("*density.png")
        Array[File] expression = glob("*expression.png")
    }
    Int mem_space = ceil(size(validate_dat, "GB") + size(models.protein_model, "GB")) + 20
    command <<< 
        Rscript ~{prediction_script} \
            --input ~{validate_dat} \
            --prefix ~{prefix}-~{models.parameter}-~{models.protein_type} \
            --pheno ~{pheno} \
            --coefs ~{models.protein_model} \
            --family ~{train_family} \
            ~{if defined(time_valid) then "--time ${time_valid}" else ""} \
            ~{if defined(quantile) then "--quantile ${quantile}" else ""} \
            ~{if defined(top) then "--top-protein ${top}" else ""} \
            ~{if defined(transform) then "--transform ${transform}" else ""} \
            ~{if defined(risk_year) then "--risk-year ${risk_year}" else ""} \
            ~{if probability then "--probability" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}


task risk_stratification{
    input{
        File stratification_script
        ProteinModel predicted_values
        String train_family
        String prefix = "PGx"
        ProteinModel models
        String? quantile
        String? absolute_risk
        String? risk_years
        String ?clinical_variables
        String ?clinical_scores
        String ?baseline_hazard
        String ?baselines
        Int seed = 12345
        Boolean equal_size = true
        String docker_image
    }
    output{
        File? risk = "${prefix}-${models.parameter}-${models.protein_type}-risk.gz"
        File? within_risk = "${prefix}-${models.parameter}-${models.protein_type}-within-risk.gz"
    }
    Int mem_space = ceil(size(models.protein_model, "GB") + 
        size(predicted_values.protein_model, "GB")) + 5
    command <<< 
        Rscript ~{stratification_script} \
            --input ~{predicted_values.protein_model} \
            --family ~{train_family} \
            --coefs ~{models.protein_model} \
            --seed ~{seed} \
            --prefix ~{prefix}-~{models.parameter}-~{models.protein_type} \
            ~{if equal_size then "--equal" else "" } \
            ~{if defined(quantile) then "--quantile ${quantile}" else ""} \
            ~{if defined(absolute_risk) then "--absolute ${absolute_risk}" else ""} \
            ~{if defined(risk_years) then "--risk-years ${risk_years}" else ""} \
            ~{if defined(baselines) then "--baseline ${baselines}" else ""} \
            ~{if defined(clinical_variables) then "--clinical ${clinical_variables}" else ""} \
            ~{if defined(clinical_scores) then "--score ${clinical_scores}" else ""} \
            ~{if defined(baseline_hazard) then "--base-hazard ${baseline_hazard}" else ""}
            
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}