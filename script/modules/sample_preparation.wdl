version 1.1
task generate_sample_file{
    input{
        File preparation_script
        File protein_expression
        File phenotype
        File? sample_map
        File? sample_grouping
        File? grs
        File? predictors
        File? protein_covariate_file
        File? keep_samples
        File? remove_samples
        String pheno_train
        String? time_to_event_train
        String? pheno_valid
        String? time_to_event_valid
        String train_family
        String valid_family 
        String? pheno_id
        String? covariates
        String? baselines
        String? clinical_scores
        String? baseline_hazard
        String? clinical_variables
        String? protein_covariates
        String grs_id = "IID"
        String grs_score = "SUM_SCORE_ALL"
        String grs_trans = "none"
        String continuous_trans = "rint"
        String binary_trans = "none"
        String prefix = "PGx"
        String? filter_valid
        String? filter_train
        String? no_trans
        Float rand_split = 0.7
        Float? censor_train
        Float? min_train
        Float? censor_valid
        Float? min_valid
        Boolean na2zero = true
        Boolean impute = true
        Boolean trans_covar = false
        Boolean trans_before_merge = false
        Boolean redistribute = false
        Boolean filter_after_trans = false
        Boolean trans_within_split = false
        Boolean quick = false
        Int seed = 12345
        String docker_image
    }
    output{
        File training = "${prefix}.training.gz"
        File valid = "${prefix}.validate.gz"
        File proteins = "${prefix}.proteins"
        File json = "${prefix}-model.json"
        File protein_cov = "${prefix}.protein-covs"
    }
    Int mem_space = ceil(size(phenotype, "GB") + 
        size(protein_expression, "GB") + 
        (if defined(predictors) then size(predictors, "GB") else 0) + 
        (if defined(grs) then size(grs, "GB") else 0) + 
        (if defined(protein_covariate_file) then size(protein_covariate_file, "GB") else 0) + 
        (if defined(sample_map) then size(sample_map, "GB") else 0) + 
        (if defined(keep_samples) then size(keep_samples, "GB") else 0) + 
        (if defined(remove_samples) then size(remove_samples, "GB") else 0) + 
        (if defined(sample_grouping) then size(sample_grouping, "GB") else 0) + 15.0)
    command <<<
        Rscript ~{preparation_script} \
            --pheno-file ~{phenotype} \
            --pheno ~{pheno_train} \
            --pheno-valid ~{pheno_valid} \
            --train-family ~{train_family} \
            --valid-family ~{valid_family} \
            --protein ~{protein_expression} \
            --binary-transform ~{binary_trans} \
            --transform ~{continuous_trans} \
            --seed ~{seed} \
            --out ~{prefix} \
            ~{if defined(pheno_id) then "--pheno-id ${pheno_id}" else "" } \
            ~{if defined(keep_samples) then "--keep ${keep_samples}" else "" } \
            ~{if defined(remove_samples) then "--remove ${remove_samples}" else "" } \
            ~{if defined(time_to_event_train) then "--time-train ${time_to_event_train}" else ""} \
            ~{if defined(time_to_event_valid) then "--time-valid ${time_to_event_valid}" else ""} \
            ~{if defined(protein_covariate_file) then "--protein-cov-file ${protein_covariate_file}" else ""} \
            ~{if defined(protein_covariates) then "--protein-cov ${protein_covariates}" else ""} \
            ~{if defined(sample_map) then "--map ${sample_map}" else ""} \
            ~{if defined(sample_grouping) then "--group ${sample_grouping}" else ""} \
            ~{if defined(grs) then "--grs ${grs}" else ""} \
            ~{if defined(grs) then "--grs-id ${grs_id}" else ""} \
            ~{if defined(grs) then "--grs-score ${grs_score}" else ""} \
            ~{if defined(grs) then "--grs-trans ${grs_trans}" else ""} \
            ~{if defined(predictors) then "--predictor ${predictors}" else ""} \
            ~{if defined(covariates) then "--covariates ${covariates}" else ""} \
            ~{if defined(baselines) then "--baseline ${baselines}" else ""} \
            ~{if defined(clinical_scores) then "--score ${clinical_scores}" else ""} \
            ~{if defined(baseline_hazard) then "--base-hazard ${baseline_hazard}" else ""} \
            ~{if defined(clinical_variables) then "--clinical ${clinical_variables}" else ""} \
            ~{if defined(filter_valid) then "--filter-valid ${filter_valid}" else ""} \
            ~{if defined(filter_train) then "--filter-train ${filter_train}" else ""} \
            ~{if defined(no_trans) then "--no-trans ${no_trans} " else ""} \
            ~{if defined(rand_split) then "--rand-split ${rand_split}" else ""} \
            ~{if na2zero then "--na2zero" else ""} \
            ~{if impute then "--impute" else ""} \
            ~{if trans_covar then "--cov-transform" else ""} \
            ~{if trans_before_merge then "--trans-before-merge" else ""} \
            ~{if redistribute then "--redistribute" else ""}  \
            ~{if filter_after_trans then "--filter-after-trans" else ""} \
            ~{if trans_within_split then "--trans-within-split" else ""} \
            ~{if quick then "--quick" else ""} \
            ~{if defined(censor_train) then "--right-censor-train ${censor_train}" else ""} \
            ~{if defined(min_train) then "--min-censor-train ${min_train}" else ""} \
            ~{if defined(censor_valid) then "--right-censor-valid ${censor_valid}" else ""} \
            ~{if defined(min_valid) then "--min-censor-valid ${min_valid}" else ""} \
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}
