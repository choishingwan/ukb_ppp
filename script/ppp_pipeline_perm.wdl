version 1.1
import "modules/sample_preparation.wdl"
import "modules/model_training.wdl"
# License for Non-Commercial Use of PGx-PPP code
# All files in this repository (“source code”) are licensed under the following
# terms below:
# "You" refers to an academic institution or academically employed full-time
# personnel only.
# "Regeneron" refers to Regeneron Pharmaceuticals, Inc.
# Regeneron hereby grants You a right to use, reproduce, modify, or distribute
# the PGx-PPP source code, in whole or in part, whether in original or modified
# form, for academic research purposes only.  The foregoing right is
# royalty-free, worldwide, revocable, non-exclusive, and non-transferable.
#
# Prohibited Uses:  The rights granted herein do not include any right to use by
# commercial entities or commercial use of any kind, including, without
# limitation, any integration into other code or software that is used for
# further commercialization, any reproduction, copy, modification or creation
# of a derivative work that is then incorporated into a commercial product or
# service or otherwise used for any commercial purpose, or distribution of the
# source code not in conformity with the restrictions set forth above, whether in
# whole or in part and whether in original or modified form, and any such
# commercial usage is not permitted.
#
# Except as expressly provided for herein, nothing in this License grants to You
# any right, title or interest in and to the intellectual property of Regeneron
# (either expressly or by implication or estoppel).  Notwithstanding anything
# else in this License, nothing contained herein shall limit or compromise the
# rights of Regeneron with respect to its own intellectual property or limit
# its freedom to practice and to develop its products and product candidates.
#
# If the source code, whole or in part and in original or modified form, is
# reproduced, shared or distributed in any manner, it must (1) identify Regeneron
# Pharmaceuticals, Inc. as the original creator, and (2) include the terms of
# this License.
#
# UNLESS OTHERWISE SEPARATELY AGREED UPON, THE SOURCE CODE IS PROVIDED ON AN
# AS-IS BASIS, AND REGENERON PHARMACEUTICALS, INC. MAKES NO REPRESENTATIONS OR
# WARRANTIES OF ANY KIND CONCERNING THE SOURCE CODE, IN WHOLE OR IN PART AND IN
# ORIGINAL OR MODIFIED FORM, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHER
# REPRESENTATIONS OR WARRANTIES. THIS INCLUDES, WITHOUT LIMITATION, WARRANTIES
# OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT,
# ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OR ABSENCE OF
# ERRORS, WHETHER OR NOT KNOWN OR DISCOVERABLE.
#
# In no case shall Regeneron be liable for any loss, claim, damage, or expenses,
# of any kind, which may arise from or in connection with this License or the
# use of the source code. You shall indemnify and hold Regeneron and its
# employees harmless from any loss, claim, damage, expenses, or liability, of
# any kind, from a third-party which may arise from or in connection with this
# License or Your use of the source code.
#
# You agree that this License and its terms are governed by the laws of the
# State of New York, without regard to choice of law rules or the United
# Nations Convention on the International Sale of Goods.
#
# Please reach out to Regeneron Pharmaceuticals Inc./Administrator relating to
# any non-academic or commercial use of the source code.

struct ProteinModel{
    String parameter
    String protein_type
    File? protein_model
}
struct Model{
    String name
    Array[String]+ independent
}

task unpack_split{
    input{
        File splits
        String docker_image
    }
    command <<<
        unzip ~{splits}
    >>>
    output{
        Array[File] split_list = glob("*")
    }
    runtime{
        container: docker_image
        cpu: 1
        memory: "1 GB"
    }
}

task combine_perm{
    input{
        Array[File] results
        String type
        String docker_image
    }
    output{
        File res = "perm-${type}-res.csv.gz"
    }
    command <<<
    R --no-save  <<CODE
    library(PGxUtil)
    library(data.table)
    inputs <- PGxUtil::strsplit0("~{sep(",", results)}", split=",")
    res <- NULL
    for(i in inputs){
        res <- rbind(res, fread(i))
    }
    fwrite(res,  "perm-~{type}-res.csv.gz")

    CODE
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "60 GB"
    }
}

task predict_summary{
    input{
        File predict_summary_script
        File validate_dat
        ProteinModel non_protein_predicted
        ProteinModel protein_predicted
        ProteinModel non_protein_models
        ProteinModel protein_models
        String pheno
        String train_family
        String prefix = "PGx"
        String split_name
        String? transform
        String? time_valid
        String? quantile
        String? risk_year
        String? sensitivity
        String docker_image
    }
    output{
        File auc = glob("*-auc.csv.gz")[0]
        Array[File]? statistic = glob("*-stat.csv.gz")
        Array[File]? proteins = glob("*-protein.csv.gz")
        Array[File]? correlation = glob("*-cor.csv.gz")
        Array[File]? calibration = glob("*-calibrate.csv.gz")
        Array[File]? recalibration = glob("*-recalibrate.csv.gz")
        Array[File]? odds_ratio = glob("*-or.csv.gz")
        Array[File]? ascertain = glob("*-ascertain.csv.gz")
        File predicted = glob("*-predicted.csv.gz")[0]
    }
    Int mem_space = max(40, ceil(size(validate_dat, "GB") +
        size(non_protein_models.protein_model, "GB")+
        size(protein_models.protein_model, "GB")+
        size(non_protein_predicted.protein_model, "GB")+
        size(protein_predicted.protein_model, "GB"))*2)
    command <<<
    Rscript ~{predict_summary_script} \
            --input ~{validate_dat} \
            --prefix ~{prefix}-~{protein_models.parameter}-~{protein_models.protein_type} \
            --pheno ~{pheno} \
            --models ~{protein_models.protein_model},~{non_protein_models.protein_model} \
            --predicted ~{protein_predicted.protein_model},~{non_protein_predicted.protein_model} \
            --family ~{train_family} \
            --split ~{split_name} \
            ~{if defined(quantile) then "--quantile ${quantile}" else ""} \
            ~{if defined(time_valid) then "--time ${time_valid}" else ""} \
            ~{if defined(risk_year) then "--risk-year ${risk_year}" else ""} \
            ~{if defined(transform) then "--transform ${transform}" else ""} \
            ~{if defined(sensitivity) then "--sensitivity ${sensitivity}" else ""}
    >>>
    runtime{
        container: docker_image
        cpu: 1
        memory: "~{mem_space} GB"
    }
}
workflow ppp_pipeline{
    parameter_meta{
        preparation_script: {
            help: "Rscript responsible for preparing the input data."
        }
        protein_selection_script:{
            help: "Rscript responsible for performing protein selection"
        }
        general_training_script:{
            help: "Rscript responsible for the non-protein model training"
        }
        coefficient_script:{
            help: "Rscript responsible for the final model training"
        }
        prediction_script:{
            help: "Rscript responsible for the generation of prediction score"
        }
        protein_expression: {
            help: "Protein expression data from Hyun. Must contain sample ID in either the `RGCpID` or `IID` column. All column with *_OID* will be assumed to be protein. If `sample_grouping` is not provided and `rand_split` is false, there should be a `Discovery` column containing the grouping information."
        }
        protein_covariate_file: {
            help: "File containing covariates that should only be used when protein expressions are included in the model. Must have same sample ID column as the protein expression file"
        }
        phenotype: {
            help: "Phenotype file. Should contain all phenotype information, e.g. phenotype, diagnosis date. Sample ID column should be provided with `pheno_id`. If sample ID is different from the protein expression file, please provide a `sample_map` file for ID mapping."
        }
        sample_map: {
            help: "A file containing sample ID maps, with `RGCpID` and the ID of the phenotype file (usually `eid`)."
        }
        sample_grouping:{
            help: "A json file contain a list of sample grouping files, each with two columns. First column should be the `RGCpID` (or name that matched to the protein expression file) and the second column should be a vector of true and false, where true = samples that should be used for training"
        }
        grs: {
            help: "The GRS file. Should contain at least two columns. The sample ID column (defined using grs_id) and the score column (defined using grs_score). When not provided, GRS will not be included in the model"
        }
        predictors:{
            help: "A file containing all required predictor(s) and covariate(s). Must have a sample ID column named `IID`. When not provided, will assume all covariates, baselines, clinical score and clinical variables are included in the phenotype file"
        }
        pheno_id: {
            help: "The column name of sample ID in the phenotype file. Default is IID"
        }
        pheno_train: {
            help: "The column name of the phenotype that should be used for training."
        }
        pheno_valid: {
            help: "The colums name of the phenotype that should be used for validation. If not provided, will assume this should be the same as pheno_train"
        }
        continuous_trans: {
            help: "Transformation to be performed on all continuous variables (except covariates unless `trans_covar` = true, and except GRS). Valid options are: `rint`, `scale` and `none`"
        }
        binary_trans: {
            help: "Transformation to be performed on all binary variables (except covariates, unless `trans_covar` = true). Valid options are `rint`, `scale` and `none`"
        }
        grs_trans: {
            help: "Transformation to be performed on the GRS. Valid options are `rint`, `scale` and `none`"
        }
        covariates: {
            help: "Covariates that should be used when training the model. Will not be used for prediction. This should be a comma separated list of column names. Can contain interation and power terms, which should be automatically taken care of by the script."
        }
        baselines: {
            help: "Covariates that should be used for both training and prediction. "
        }
        clinical_scores: {
            help: "Column contain established clinical scores. This variable will not undergo any transformation and will used as is in the model (i.e. assume coeffcient of 1)"
        }
        clinical_variables:{
            help: "Columns containing relevant clinical variables. These variables will be transformed and fit using the training data."
        }
        protein_covariates: {
            help: "Columns found in the protein_covariate_file that will only be used when protein expressions are included in the model. These will not be used for prediction"
        }
        grs_id: {
            help: "Column in the grs file that represent the sample ID. Assume this will match to RGCpID a.k.a. IID"
        }
        grs_score: {
            help: "Column in the grs file that contain the GRS"
        }
        rand_split:{
            help: "Boolean to indicate if we will randomly split our sample into two halve for training and validation"
        }
        na2zero:{
            help: "Boolean indicate if we should treat any NA binary variables as 0 (no event)"
        }
        impute:{
            help: "Boolean indicate if we should impute missing continuous variables"
        }
        trans_covar:{
            help: "Boolean indicate if we should transform the covariates."
        }
        is_longitudinal: {
            help: "Boolean indicate if input is longitudinal. If true, will assume the `FlUp` column is the time to event"
        }
        trans_before_merge: {
            help: "Boolean indicate if transformation should be performed before or after merging all data. If false, we will perform transformation within each input file independently. E.g. Transformation GRS across all samples with GRS score, even when maybe half of the GRS samples were not found in the PPP data"
        }
        subgroup:{
            help: "Provide column name to subgroups so that we can provide the subgroup analyses results"
        }
        quantile: {
            help: "Comma separated list of Top x quantiles to be determined as extreme"
        }
        filter_valid: {
            help: "Additional filtering on validated samples. Format is Column:Value. For example, PRV_FL:1 means we should remove any samples with PRV_FL equals to 1 in our validate dataset"
        }
        filter_secondary: {
            help: "Additional filtering on validated samples. Format is Column:Value. For example, PRV_FL:1 means we should remove any samples with PRV_FL equals to 1 in our validate dataset"
        }
        filter_train: {
            help: "Additional filtering on training samples. Format is Column:Value. For example, PRV_FL:1 means we should remove any samples with PRV_FL equals to 1 in our training dataset"
        }
        absolute_risk: {
            help: "Comma separated list of absolute risk thresholds in %"
        }
        risk_years:{
            help: "Comma separated list of risk years for cox model prediction"
        }
        prefix: {
            help: "Output prefix for this run"
        }
        no_trans:{
            help: "Covariates / risk factors that should not be transformed"
        }
        protein_coef_source: {
            help: "Consensus Protein coefficient's source. Default is \"Proteins\", which corresponds to the protein only model (no baseline). You can use other models, such as Clinical+GRS+Proteins for the clinical risk plus GRS model. Will error out if such model does not exists"
        }
        min_censor_train:{
            help: "Minimal year to consider as incident (training)"
        }
        censor_train: {
            help: "year to right censor to in training data"
        }
        censor_valid: {
            help: "year to right censor to in validation data"
        }
        min_censor_valid:{
            help: "Minimal year to consider as incident (validation)"
        }
        cv_fold: {
            help: "Number of cv fold used for glmnet training"
        }
        thread: {
            help: "Number of thread used for protein selection"
        }
        seed: {
            help: "Random seed used for any randomized operation"
        }
        remove_left_censor_train:{
            help: "If true, left censored samples is removed from training. Otherwise, their incidence status will be set to 0"
        }
        remove_left_censor_valid:{
            help: "If true, left censored samples is removed from validation. Otherwise, their incidence status will be set to 0"
        }
        run_time_dependent:{
            help: "If true, run time dependent hazard ratio analysis. This can be time consuming"
        }
        sensitivity:{
            help: "List of timepoint to remove cases from analyses as a sensitivity analyses"
        }
        docker_image:{
            help: "Docker image containing the required packages for the analyses to run"
        }
    }
    input{
        File preparation_script
        File protein_selection_script
        File general_training_script
        File coefficient_script
        File prediction_script
        File predict_summary_script
        File protein_expression
        File phenotype
        File? sample_map
        File sample_grouping
        File? protein_covariate_file
        File? grs
        File? predictors
        File? keep_samples
        File? remove_samples
        String docker_image
        String pheno_train
        String? time_to_event_train
        String? pheno_valid
        String? time_to_event_valid
        String? pheno_secondary
        String? time_to_event_secondary
        String pheno_id = "IID"
        String continuous_trans = "rint"
        String binary_trans = "none"
        String grs_trans = "none"
        String? baselines
        String? covariates
        String? clinical_scores
        String? clinical_variables
        String? protein_covariates
        String protein_coef_source = "Proteins"
        String grs_id = "IID"
        String grs_score = "SUM_SCORE_ALL"
        String prefix = "PGx"
        String? quantile
        String? risk_years
        String? filter_valid
        String? filter_train
        String? filter_secondary
        String? no_trans
        String train_family = "logistic"
        String? secondary_family
        String? valid_family
        String? baseline_hazard
        String? sensitivity
        Float rand_split = 0.7
        Boolean na2zero = true
        Boolean impute = true
        Boolean trans_covar = false
        Boolean trans_before_merge = false
        Boolean redistribute = false
        Boolean filter_after_trans = false
        Boolean trans_within_split = false
        Int seed = 12345
        Int thread = 1
        # Allow optional, as we don't always need this
        Int? cv_fold
        Float? censor_train
        Float? censor_valid
        Float? censor_secondary
        Float? min_train
        Float? min_valid
        Float? min_secondary
        Boolean? probability_score
    }
    ##################################################################
    #                  Start of pipeline scripts                     #
    ##################################################################
    String validation_family = select_first([valid_family, train_family])
    String validation_phenotype = select_first([pheno_valid, pheno_train])
    String secondary_family_in = select_first([secondary_family, train_family])

    call unpack_split{
        input:
            splits = sample_grouping,
            docker_image = docker_image
    }
    Array[Int] index = range(length(unpack_split.split_list))
    scatter( idx in index){
        call sample_preparation.generate_sample_file{
            input:
                preparation_script = preparation_script,
                protein_expression = protein_expression,
                protein_covariate_file = protein_covariate_file,
                phenotype = phenotype,
                predictors = predictors,
                sample_map = sample_map,
                sample_grouping = unpack_split.split_list[idx],
                grs = grs,
                pheno_train = pheno_train,
                time_to_event_train = time_to_event_train,
                pheno_valid = validation_phenotype,
                time_to_event_valid = time_to_event_valid,
                pheno_id = pheno_id,
                continuous_trans = continuous_trans,
                binary_trans = binary_trans,
                grs_trans = grs_trans,
                covariates = covariates,
                baselines = baselines,
                clinical_scores = clinical_scores,
                baseline_hazard = baseline_hazard,
                clinical_variables = clinical_variables,
                protein_covariates = protein_covariates,
                grs_id = grs_id,
                grs_score = grs_score,
                prefix = prefix,
                filter_valid = filter_valid,
                filter_train = filter_train,
                rand_split = rand_split,
                na2zero = na2zero,
                impute = impute,
                trans_covar = trans_covar,
                train_family = train_family,
                valid_family = validation_family,
                trans_before_merge = trans_before_merge,
                no_trans = no_trans,
                remove_samples = remove_samples,
                keep_samples = keep_samples,
                redistribute = redistribute,
                filter_after_trans = filter_after_trans,
                trans_within_split = trans_within_split,
                seed = seed,
                censor_train = censor_train,
                min_train = min_train,
                censor_valid = censor_valid,
                min_valid = min_valid,
                quick = true,
                docker_image = docker_image
        }
        if(defined(pheno_secondary)){
            call sample_preparation.generate_sample_file as secondary_sample{
                input:
                    preparation_script = preparation_script,
                    protein_expression = protein_expression,
                    protein_covariate_file = protein_covariate_file,
                    phenotype = phenotype,
                    predictors = predictors,
                    sample_map = sample_map,
                    sample_grouping = unpack_split.split_list[idx],
                    grs = grs,
                    pheno_train = pheno_train,
                    time_to_event_train = time_to_event_train,
                    pheno_valid = pheno_secondary,
                    time_to_event_valid = time_to_event_secondary,
                    pheno_id = pheno_id,
                    continuous_trans = continuous_trans,
                    binary_trans = binary_trans,
                    grs_trans = grs_trans,
                    covariates = covariates,
                    baselines = baselines,
                    clinical_scores = clinical_scores,
                    baseline_hazard = baseline_hazard,
                    clinical_variables = clinical_variables,
                    protein_covariates = protein_covariates,
                    grs_id = grs_id,
                    grs_score = grs_score,
                    prefix = prefix,
                    filter_valid = filter_secondary,
                    filter_train = filter_train,
                    rand_split = rand_split,
                    na2zero = na2zero,
                    impute = impute,
                    trans_covar = trans_covar,
                    train_family = train_family,
                    valid_family = secondary_family_in,
                    trans_before_merge = trans_before_merge,
                    no_trans = no_trans,
                    remove_samples = remove_samples,
                    keep_samples = keep_samples,
                    redistribute = redistribute,
                    filter_after_trans = filter_after_trans,
                    trans_within_split = trans_within_split,
                    seed = seed,
                    censor_train = censor_train,
                    min_train = min_train,
                    censor_valid = censor_secondary,
                    min_valid = min_secondary,
                    quick = true,
                    docker_image = docker_image
            }
        }
        ##################################################################
        #               Non-protein model training                       #
        ##################################################################
        call model_training.non_protein_model_training{
            input:
                general_training_script = general_training_script,
                training_dat = generate_sample_file.training,
                pheno_train = pheno_train,
                covariates = covariates,
                baselines = baselines,
                clinical_scores = clinical_scores,
                clinical_variables = clinical_variables,
                train_family = train_family,
                time_to_event_train = time_to_event_train,
                prefix = prefix,
                docker_image = docker_image
        }
        ProteinModel non_protein_model_store = ProteinModel{
            parameter: "NA",
            protein_type: "NA",
            protein_model: non_protein_model_training.non_protein_model
        }
        ##################################################################
        #            Fit coefficients for non-protein model              #
        ##################################################################
        call model_training.generate_predictions as non_protein_prediction{
            input:
                prediction_script  = prediction_script,
                models = non_protein_model_store,
                pheno = validation_phenotype,
                validate_dat = generate_sample_file.valid,
                train_family = train_family,
                prefix = prefix,
                quantile = quantile,
                risk_year = risk_years,
                probability = probability_score,
                docker_image = docker_image
        }
        ProteinModel non_protein_prediction_model = ProteinModel{
            parameter: "NA",
            protein_type: "NA",
            protein_model: if non_protein_prediction.predicted == None then None else non_protein_prediction.predicted
        }
        ##################################################################
        #                   Perform protein selection                    #
        ##################################################################
        Array[Model]+ protein_models = read_json(generate_sample_file.json)
        Int num_models = length(protein_models)
        scatter(model in protein_models){
            call model_training.protein_selection{
                input:
                    protein_selection_script = protein_selection_script,
                    cur_model = model,
                    training_dat = generate_sample_file.training,
                    protein_names = generate_sample_file.proteins,
                    protein_cov = generate_sample_file.protein_cov,
                    pheno_train = pheno_train,
                    time_train = time_to_event_train,
                    train_family = train_family,
                    covariates = covariates,
                    prefix = prefix,
                    thread = thread,
                    cv_fold = cv_fold,
                    seed = seed,
                    docker_image = docker_image
            }
        }
        ##################################################################
        #               Refit protein scores to get coef                 #
        ##################################################################
        if(select_all(protein_selection.coef) != None){
            Array[File] protein_coefficients  = select_all(protein_selection.coef)
            call model_training.train_protein_score_coefficients{
                input:
                    protein_models_coef = protein_coefficients,
                    coefficient_script = coefficient_script,
                    training_dat = generate_sample_file.training,
                    protein_cov = generate_sample_file.protein_cov,
                    covariates = covariates,
                    baselines = baselines,
                    clinical_scores = clinical_scores,
                    clinical_variables = clinical_variables,
                    pheno_train = pheno_train,
                    time_train = time_to_event_train,
                    train_family = train_family,
                    prefix = prefix,
                    parameter = "cfs",
                    protein_type = "model-specific",
                    protein_coef_source = protein_coef_source,
                    num_models = num_models,
                    transform = continuous_trans,
                    docker_image = docker_image
            }
            if(train_protein_score_coefficients.model != None){
                ProteinModel cur_protein_model  = ProteinModel{
                    parameter: "cfs",
                    protein_type: "model-specific",
                    protein_model: train_protein_score_coefficients.model
                    }
                ##################################################################
                #        Get the predicted value based on trained models         #
                ##################################################################
                call model_training.generate_predictions as protein_prediction{
                    input:
                        prediction_script  = prediction_script,
                        models = cur_protein_model,
                        pheno = validation_phenotype,
                        validate_dat = generate_sample_file.valid,
                        train_family = train_family,
                        time_valid = time_to_event_valid,
                        prefix = prefix,
                        quantile = quantile,
                        transform = continuous_trans,
                        risk_year = risk_years,
                        probability = probability_score,
                        docker_image = docker_image
                }
                ProteinModel protein_prediction_model = ProteinModel{
                    parameter: "cfs",
                    protein_type: "model-specific",
                    protein_model: protein_prediction.predicted
                }
                call predict_summary{
                    input:
                        predict_summary_script = predict_summary_script,
                        validate_dat =  generate_sample_file.valid,
                        non_protein_models = non_protein_model_store,
                        protein_models = cur_protein_model,
                        non_protein_predicted = non_protein_prediction_model,
                        protein_predicted = protein_prediction_model,
                        pheno = validation_phenotype,
                        train_family = train_family,
                        prefix = prefix,
                        split_name = unpack_split.split_list[idx],
                        transform = continuous_trans,
                        time_valid = time_to_event_valid,
                        quantile = quantile,
                        risk_year = risk_years,
                        sensitivity = sensitivity,
                        docker_image = docker_image
                }
                if(secondary_sample.valid != None){
                    call model_training.generate_predictions as sec_non_protein_prediction{
                        input:
                            prediction_script  = prediction_script,
                            models = non_protein_model_store,
                            pheno = select_first([pheno_secondary])    ,
                            validate_dat = select_first([secondary_sample.valid]),
                            train_family = train_family,
                            prefix = prefix,
                            quantile = quantile,
                            risk_year = risk_years,
                            probability = probability_score,
                            docker_image = docker_image
                    }
                    ProteinModel sec_non_protein_prediction_model = ProteinModel{
                        parameter: "NA",
                        protein_type: "NA",
                        protein_model: if sec_non_protein_prediction.predicted == None then None else sec_non_protein_prediction.predicted
                    }
                    call model_training.generate_predictions as secondary_protein_prediction{
                        input:
                            prediction_script  = prediction_script,
                            models = cur_protein_model,
                            pheno = select_first([pheno_secondary]),
                            validate_dat = select_first([secondary_sample.valid]),
                            train_family = train_family,
                            time_valid = time_to_event_secondary,
                            prefix = prefix,
                            quantile = quantile,
                            transform = continuous_trans,
                            risk_year = risk_years,
                            probability = probability_score,
                            docker_image = docker_image
                    }
                    ProteinModel sec_protein_prediction_model = ProteinModel{
                        parameter: "cfs",
                        protein_type: "model-specific",
                        protein_model: secondary_protein_prediction.predicted
                    }
                    call predict_summary as secondary_summary{
                        input:
                            predict_summary_script = predict_summary_script,
                            validate_dat =  select_first([secondary_sample.valid]),
                            non_protein_models = non_protein_model_store,
                            protein_models = cur_protein_model,
                            non_protein_predicted = sec_non_protein_prediction_model,
                            protein_predicted = sec_protein_prediction_model,
                            pheno = select_first([pheno_secondary]),
                            train_family = train_family,
                            prefix = prefix,
                            split_name = unpack_split.split_list[idx],
                            transform = continuous_trans,
                            time_valid = time_to_event_secondary,
                            quantile = quantile,
							risk_year = risk_years,
                            sensitivity = sensitivity,
                            docker_image = docker_image
                    }
                }
            # This has protein prediction and non-protein prediction
            }
        }
    # End of current perm
    }

    call combine_perm as combine_predicted{
        input:
            results = select_all(predict_summary.predicted),
            type = "predict",
            docker_image = docker_image
    }
    call combine_perm as combine_auc{
        input:
            results = select_all(predict_summary.auc),
            type = "auc",
            docker_image = docker_image
    }
    call combine_perm as combine_or{
      input:
          results = flatten(select_all(predict_summary.odds_ratio)),
          type = "odds_ratio",
          docker_image = docker_image
    }
    if(length(select_all(secondary_summary.auc)) > 0){
        call combine_perm as combine_secondary_auc{
            input:
                results = select_all(secondary_summary.auc),
                type = "secondary-auc",
                docker_image = docker_image
        }
        call combine_perm as combine_secondary_or{
          input:
                results =flatten(select_all(secondary_summary.odds_ratio)),
                type = "secondary-or",
                docker_image = docker_image
        }
        call combine_perm as combine_secondary_predict{
            input:
                results = select_all(secondary_summary.predicted),
                type = "secondary-predict",
                docker_image = docker_image
        }
        call combine_perm as combine_secondary_stat{
            input:
                results = flatten(select_all(secondary_summary.statistic)),
                type = "secondary-stat",
                docker_image = docker_image
        }
        call combine_perm as combine_secondary_ascertain{
            input:
                results = flatten(select_all(secondary_summary.ascertain)),
                type = "secondary-stat",
                docker_image = docker_image
        }
    }
    call combine_perm as combine_protein{
        input:
            results = flatten(select_all(predict_summary.proteins)),
            type = "protein",
            docker_image = docker_image
    }
    call combine_perm as combine_stat{
        input:
            results = flatten(select_all(predict_summary.statistic)),
            type = "stat",
            docker_image = docker_image
    }
    call combine_perm as combine_ascertain{
        input:
            results = flatten(select_all(predict_summary.ascertain)),
            type = "stat",
            docker_image = docker_image
    }
    call combine_perm as combine_calibrate{
        input:
            results = flatten(select_all(predict_summary.calibration)),
            type = "calibrate",
            docker_image = docker_image
    }
    call combine_perm as combine_recalibrate{
        input:
            results = flatten(select_all(predict_summary.recalibration)),
            type = "recalibrate",
            docker_image = docker_image
    }
    call combine_perm as combine_cor{
        input:
            results = flatten(select_all(predict_summary.correlation)),
            type = "cor",
            docker_image = docker_image
    }
    output{
        File coorelation  = combine_cor.res
        File statistic = combine_stat.res
        File proteins = combine_protein.res
        File auc = combine_auc.res
        File predicted = combine_predicted.res
        File calibrate = combine_calibrate.res
        File recalibrate = combine_recalibrate.res
        File ascertain = combine_ascertain.res
        File odds_ratio = combine_or.res
        File? secondary_auc = combine_secondary_auc.res
        File? secondary_stat = combine_secondary_stat.res
        File? secondary_ascertain = combine_secondary_ascertain.res
        File? secondary_predict = combine_secondary_predict.res
        File? secondary_odds_ratio = combine_secondary_or.res
    }
}

