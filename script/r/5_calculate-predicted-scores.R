# License for Non-Commercial Use of PGx-PPP code
# All files in this repository (“source code”) are licensed under the following
# terms below:
# "You" refers to an academic institution or academically employed full-time
# personnel only.
# "Regeneron" refers to Regeneron Pharmaceuticals, Inc.
# Regeneron hereby grants You a right to use, reproduce, modify, or distribute
# the PGx-PPP source code, in whole or in part, whether in original or modified
# form, for academic research purposes only.  The foregoing right is
# royalty-free, worldwide, revocable, non-exclusive, and non-transferable.
#
# Prohibited Uses:  The rights granted herein do not include any right to use by
# commercial entities or commercial use of any kind, including, without
# limitation, any integration into other code or software that is used for
# further commercialization, any reproduction, copy, modification or creation
# of a derivative work that is then incorporated into a commercial product or
# service or otherwise used for any commercial purpose, or distribution of the
# source code not in conformity with the restrictions set forth above, whether in
# whole or in part and whether in original or modified form, and any such
# commercial usage is not permitted.
#
# Except as expressly provided for herein, nothing in this License grants to You
# any right, title or interest in and to the intellectual property of Regeneron
# (either expressly or by implication or estoppel).  Notwithstanding anything
# else in this License, nothing contained herein shall limit or compromise the
# rights of Regeneron with respect to its own intellectual property or limit
# its freedom to practice and to develop its products and product candidates.
#
# If the source code, whole or in part and in original or modified form, is
# reproduced, shared or distributed in any manner, it must (1) identify Regeneron
# Pharmaceuticals, Inc. as the original creator, and (2) include the terms of
# this License.
#
# UNLESS OTHERWISE SEPARATELY AGREED UPON, THE SOURCE CODE IS PROVIDED ON AN
# AS-IS BASIS, AND REGENERON PHARMACEUTICALS, INC. MAKES NO REPRESENTATIONS OR
# WARRANTIES OF ANY KIND CONCERNING THE SOURCE CODE, IN WHOLE OR IN PART AND IN
# ORIGINAL OR MODIFIED FORM, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHER
# REPRESENTATIONS OR WARRANTIES. THIS INCLUDES, WITHOUT LIMITATION, WARRANTIES
# OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT,
# ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OR ABSENCE OF
# ERRORS, WHETHER OR NOT KNOWN OR DISCOVERABLE.
#
# In no case shall Regeneron be liable for any loss, claim, damage, or expenses,
# of any kind, which may arise from or in connection with this License or the
# use of the source code. You shall indemnify and hold Regeneron and its
# employees harmless from any loss, claim, damage, expenses, or liability, of
# any kind, from a third-party which may arise from or in connection with this
# License or Your use of the source code.
#
# You agree that this License and its terms are governed by the laws of the
# State of New York, without regard to choice of law rules or the United
# Nations Convention on the International Sale of Goods.
#
# Please reach out to Regeneron Pharmaceuticals Inc./Administrator relating to
# any non-academic or commercial use of the source code.
#
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(PGxUtil))
suppressPackageStartupMessages(library(optparse))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(forcats))

# helper functions --------------------------------------------------------

.d <- `[`


#' Function to calculate the quantiles
#' So far, this offer zero check on input. This also does not do any
#' special treatment on ties
#' @param x input numeric vector
#' @param num.quant number of quantiles required
#'
#' @return
#' @export
#'
#' @examples
get_quantile <- function(x, num.quant) {
  quant <- as.numeric(cut(x,
                          breaks = unique(quantile(
                            x, probs = seq(0, 1, 1 / num.quant)
                          )),
                          include.lowest = TRUE))
  return(quant)
}


protein_expression_plot <-
  function(input,
           prefix,
           name,
           quant,
           phenotype,
           cbbPalette,
           is_longitudinal,
           is_consensus) {
    num_quant <- 100 / quant
    cur_plot_dat <-
      input[, Quantile := get_quantile(value, num_quant), by = variable] |>
      .d(
        ,
        .(
          Mean = mean(get(phenotype)) * 100,
          CI = qnorm(0.025, lower.tail = F) *
            sqrt((mean(get(
              phenotype
            ))) * (1 - mean(get(
              phenotype
            ))) / .N) * 100,
          coef = unique(abs(Estimate))
        ),
        by = c("Quantile", "variable")
      ) |>
      .d(, Protein := sapply(variable, function(x) {
        minus <- ifelse(x %like% "v1$" | x %like% "v2$", -2, -1)
        PGxUtil::strsplit0(as.character(x), split = "_") |>
          head(n = minus) |>
          paste(sep = "-", collapse = "-")
      })) |>
      .d(, Protein := fct_reorder(as.factor(Protein), -abs(coef)))
    cur_plot <-
      ggplot(cur_plot_dat,
             aes(
               x = Quantile,
               y = Mean,
               ymin = Mean - CI,
               ymax = Mean + CI,
               color = Protein
             )) +
      labs(y = ifelse(is_longitudinal,
                      "Incidence rate (%)",
                      "% Prevalence")) +
      geom_pointrange(size = 1) +
      geom_line(size = 1.5) +
      facet_wrap( ~ Protein, ncol = 5) +
      scale_color_manual(values = cbbPalette) +
      scale_x_continuous(breaks = seq(1, num_quant)) +
      theme_bw() +
      theme(
        axis.title = element_text(size = 16, face = "bold"),
        axis.title.x = element_blank(),
        axis.text  = element_text(size = 14),
        axis.text.x = element_text(angle = 45, hjust = 1),
        panel.grid = element_blank(),
        strip.background = element_blank(),
        strip.text = element_text(size = 16, face = "bold"),
        legend.position = "none"
      )
    ggsave(
      paste0(
        prefix,
        ifelse(is_consensus, "", paste0("-", name)),
        "-",
        num_quant,
        "-expression.png"
      ),
      plot = cur_plot,
      height = 3.5 * ceiling(num_top / 5),
      width = min(15, num_quant * 3)
    )
  }

# parameters --------------------------------------------------------------

option_list <-
  list(
    make_option(c("--family"),
                type = "character",
                help = "Data family of the training sample. Support cox, binomial, binary, logistic, continuous and gaussian"),
    make_option(
      c("--input"),
      type = "character",
      help  = "File containing the phenotype and other variables for the validation samples",
      default = NULL
    ),
    make_option(
      c("--quantile"),
      type = "character",
      help  = "comma separated threshold to define the top X quantiles",
      default = NULL
    ),
    make_option(
      c("--seed"),
      type = "numeric",
      help = "Use for random number generation. Mainly used to break ties in quantile splits",
      default = 12345
    ),
    make_option(
      c("--pheno"),
      type = "character",
      help = "Column name containing the phenotype of interest.",
      default = NULL
    ),
    make_option(
      c("--top-protein"),
      type = "numeric",
      help = "Number top proteins to be included in the figure",
      default = 10,
      dest = "top"
    ),
    make_option(
      c("--coefs"),
      type = "character",
      help = "RDS file containing the trained models",
      default = NULL
    ),
    make_option(
      c("--prefix"),
      type = "character",
      help = "Output prefix",
      default = "PGx"
    ),
    make_option(c("--time"),
                type = "character",
                help = "Column name of the time to event information"),
    make_option(
      c("--plots"),
      action = "store_true",
      help = "Whether to generate plots",
      default = FALSE
    ),
    make_option(
      c("--transform"),
      type = "character",
      help = "Transformation to be performed on the protein score. This is to ensure consistency in interpretation of the coefficients",
      default = "rint"
    ),
    make_option(
      c("--probability"),
      help = "Calculate probability score instead of normal predict score",
      action = "store_true",
      default = FALSE
    ),
    make_option(
      c("--risk-year"),
      dest = "risk",
      type = "character",
      help = "Risk year for cox models",
      default = NULL
    )
  )

# Parameter check ---------------------------------------------------------

argv <- parse_args(OptionParser(option_list = option_list))
argv$family <- PGxUtil::model_parse(tolower(argv$family))
error  <- FALSE
message <- ""
if (is.null(argv$time) && argv$family == "cox") {
  argv$time <- "FlUp"
}
if (is.null(argv$input)) {
  error <- TRUE
  message <-
    paste(message, "Required input file not provided", sep = "\n")
}
if (is.null(argv$pheno)) {
  error <- TRUE
  message <-
    paste(message, "Required phenotype name", sep = "\n")
}
if (is.null(argv$coefs)) {
  error <- TRUE
  message <-
    paste(message, "Required the model RDS file", sep = "\n")
}
quantiles <- NULL
if (!is.null(argv$quantile)) {
  quantiles <-
    as.numeric(PGxUtil::strsplit0(argv$quantile, split = ","))
}

if (!is.null(quantiles) && any(quantiles > 100 | quantiles < 1)) {
  error <- TRUE
  message <-
    paste(message, "Quantiles must be between 1-100", sep = "\n")
}

if (argv$probability &&
    argv$family == "cox" && is.null(argv$risk)) {
  error <- TRUE
  message <-
    paste(message,
          "Must provide risk year for cox model if probability score is required",
          sep = "\n")
}
argv$risk <- as.numeric(argv$risk)
if (is.na(argv$risk)) {
  stop("Risk year must be a numeric value")
}

transform <- PGxUtil::trans_func(argv$transform)
num_top <- argv$top
if (error) {
  stop(message)
}

# Read in data ------------------------------------------------------------
sample_dat <- data.table::fread(argv$input, header = TRUE)
if (!is.null(argv$time) && !argv$time %in% colnames(sample_dat)) {
  stop(paste0("Time to event column `", argv$time, "` not found in input."))
}
# Phenotype is only for plotting, so we don't need the time to event object
phenotype <- argv$pheno

models <- readRDS(argv$coefs)
is_consensus <- TRUE
prev_proteins <- NULL
for (i in seq_along(models)) {
  if (!is.null(models[[i]]$proteins)) {
    if (is.null(prev_proteins)) {
      prev_proteins <- models[[i]]$proteins
    } else{
      if ((nrow(prev_proteins) != nrow(models[[i]]$proteins)) ||
          (ncol(prev_proteins) != ncol(models[[i]]$proteins)) ||
          !all(prev_proteins == models[[i]]$proteins)) {
        is_consensus <- FALSE
        break
      }
    }
  }
}
result_dat <- NULL
covariates <- NULL
plotted_expression <- FALSE
cbbPalette <-
  c(
    "#000000",
    "#E69F00",
    "#56B4E9",
    "#009E73",
    "#F0E442",
    "#0072B2",
    "#D55E00",
    "#CC79A7",
    "#999999"
  )
if (length(cbbPalette) < num_top) {
  ori <- cbbPalette
  num_palette <- length(ori)
  num_more <- num_top - num_palette
  dup_times <- ceiling(num_more / num_palette)
  cbbPalette <- rep(cbbPalette, dup_times + 1)
}


# Start calculate predicted score -----------------------------------------

for (i in seq_along(models)) {
  cur_dat <- copy(sample_dat)
  if (!is.null(models[[i]]$proteins)) {
    cur_dat <-
      cur_dat[, ProteinScore := PGxUtil::calculate_predicted_scores(
        input = .SD,
        coefficients = models[[i]]$proteins,
        transform = transform
      )]
  }
  # Might seems odd I repeat the if case here. Main reason is that the first part
  # is required in all situation, whereas the second part of this if case is
  # only focusing on plotting, which I usually want to skip in manual testing
  # of the pipeline
  if (!is.null(models[[i]]$proteins) && argv$plots) {
    # Try to plot the distribution of the ProteinScore, just in case if we
    # have a very small distribution that can lead to problem with quantile cuts
    dist.plot <- ggplot(data = cur_dat, aes(x = ProteinScore)) +
      geom_density() +
      labs(x = "Composite Protein Score",
           caption = names(models)[i],
           y = "Density") +
      theme_bw() +
      theme(axis.title = element_text(size = 16, face = "bold"),
            axis.text = element_text(size = 14))
    ggsave(
      paste0(argv$prefix, "-", names(models)[i], "-density.png"),
      plot = dist.plot,
      height = 7,
      width = 7
    )

    # Also do protein expression plot -----------------------------------------
    if (!is.null(quantiles)) {
      # Check if we have new proteins
      cur_top <-
        tail(models[[i]]$proteins[order(abs(Estimate))], n = num_top)
      if (!plotted_expression || !is_consensus) {
        plotted_expression <- TRUE
        # now do the analyses
        plot_dat <-
          sample_dat[, c(phenotype, cur_top$proteins), with = FALSE] |>
          melt(id.vars = phenotype) |>
          merge(models[[i]]$proteins[, c("variable", "Estimate")], by = "variable")
        for (quant in quantiles) {
          protein_expression_plot(
            input = plot_dat,
            prefix = argv$prefix,
            name = names(models)[i],
            quant = quant,
            phenotype = phenotype,
            cbbPalette = cbbPalette,
            is_longitudinal = (argv$family == "cox"),
            is_consensus = is_consensus
          )
        }
      }
    }
  }
  cur_dat[, Predicted :=
            PGxUtil::calculate_predicted_scores(
              input = .SD,
              coefficients = models[[i]]$model[covariate == FALSE],
              basehazard = models[[i]]$basehazard,
              risk_year = argv$risk,
              probability = argv$probability
            )] |>
    .d(, model := names(models)[i])
  if (!all(is.na(cur_dat[, Predicted]))) {
    result_dat <- rbind(result_dat, cur_dat)
  }
  if (!is.null(models[[i]]$covariates)) {
    covariates <- c(covariates, models[[i]]$covariates)
  }
}

downstream <-
  c("IID", argv$pheno, unique(covariates), argv$baseHazard)
if (argv$family == "cox") {
  downstream <- unique(c(downstream, argv$time, argv$pheno))
}
# output results ----------------------------------------------------------
result_dat <-
  merge(result_dat, sample_dat[, downstream, with = FALSE])
fwrite(result_dat, paste0(argv$prefix, ".predicted.gz"))

# Find the full model -----------------------------------------------------
# My general conception is that the following is rather useless, but we will
# keep it here until we have fully developed a better approach e.g. log
# likelihood test that also works on cox model?
#
#
# We might want the PPV of each individual component, for that, we need the
# prediction score calculated using each individual component
#
all.models <- names(models)
protein_models <-
  # Ignore protein only model here
  data.table(models = all.models[grepl("Proteins", all.models) &
                                   all.models != "Proteins"]) |>
  .d(, is_score := grepl("Score", models)) |>
  .d(, is_clinical := grepl("Clinical", models)) |>
  .d(, is_grs := grepl("GRS", models)) |>
  .d(, num_component := length(PGxUtil::strsplit0(models, split = "\\+")), by = "models")
# Only make sense extract the individual contribution when we do have a full
# model
if (!is.null(protein_models) &&  nrow(protein_models) > 0) {
  # We also want models without the GRS, as we don't usually have information
  # on that
  #
  required_models <- NULL
  for (has_grs in c(TRUE, FALSE)) {
    clinical_protein <-
      protein_models[is_clinical == TRUE & is_grs == has_grs]
    if (nrow(clinical_protein) > 0) {
      clinical_protein <-
        clinical_protein[num_component == max(num_component)] |>
        .d(, model_type := "Risk_Factors")
    }
    if (nrow(clinical_protein) > 0) {
      required_models <- rbind(required_models, clinical_protein)
    }
    score_protein <-
      protein_models[is_score == TRUE &
                       is_grs == has_grs]
    if (nrow(score_protein) > 0) {
      score_protein <-
        score_protein[num_component == max(num_component)] |>
        .d(, model_type := "Clinical_Score")
    }
    if (nrow(score_protein) > 0) {
      required_models <- rbind(required_models, score_protein)
    }
  }
  required_models <- unique(required_models)
  if (is.null(required_models)) {
    # This is possible when we have no clinical risk or clinical score. In this
    # case, try and see if we have GRS and baseline model
    for (has_grs in c(TRUE, FALSE)) {
      baseline_protein <-
        protein_models[is_grs == has_grs]
      if (nrow(baseline_protein) > 0) {
        baseline_protein <-
          baseline_protein[num_component == max(num_component)] |>
          .d(, model_type := "Baseline")
      }
      if (nrow(baseline_protein) > 0) {
        required_models <- rbind(required_models, baseline_protein)
      }
    }
  }
  required_models <- unique(required_models)
  # To do within model split, we need the baseline and clinical information as we
  # need to group the variables.
  # Should always have something here, otherwise protein_models will be empty
  within_model_prediction <- NULL
  for (idx in seq_len(nrow(required_models))) {
    model_name <- required_models[, models][idx]
    info <- required_models[idx, c("is_grs", "model_type")]
    independent <- models[[model_name]]$independent
    # We must have protein score
    cur_dat <-
      sample_dat[, ProteinScore := PGxUtil::calculate_predicted_scores(
        input = .SD,
        coefficients = models[[model_name]]$proteins,
        transform = transform
      )]
    for (cur_var in  unlist(lapply(independent, PGxUtil::parse_covariate))) {
      cur_predict <- copy(cur_dat)[, Predicted := PGxUtil::calculate_predicted_scores(
        input = .SD,
        coefficients = models[[model_name]]$model[variable %in% cur_var],
        basehazard = models[[model_name]]$basehazard,
        risk_year = argv$risk,
        probability = argv$probability
      )] |>
        .d(, model := model_name) |>
        .d(, model_component := cur_var) |>
        .d(, c("is_grs", "model_type") := info)
      within_model_prediction <-
        rbind(within_model_prediction, cur_predict)
    }
  }
  if (!is.null(within_model_prediction)) {
    # Remove NAs from the within model prediction
    within_model_prediction <- na.omit(within_model_prediction)
    within_model_prediction <-
      merge(within_model_prediction, sample_dat[, downstream, with = FALSE])
    fwrite(within_model_prediction,
           paste0(argv$prefix, ".within-predicted.gz"))
  }
}
