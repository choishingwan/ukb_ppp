# License for Non-Commercial Use of PGx-PPP code
# All files in this repository (“source code”) are licensed under the following
# terms below:
# "You" refers to an academic institution or academically employed full-time
# personnel only.
# "Regeneron" refers to Regeneron Pharmaceuticals, Inc.
# Regeneron hereby grants You a right to use, reproduce, modify, or distribute
# the PGx-PPP source code, in whole or in part, whether in original or modified
# form, for academic research purposes only.  The foregoing right is
# royalty-free, worldwide, revocable, non-exclusive, and non-transferable.
#
# Prohibited Uses:  The rights granted herein do not include any right to use by
# commercial entities or commercial use of any kind, including, without
# limitation, any integration into other code or software that is used for
# further commercialization, any reproduction, copy, modification or creation
# of a derivative work that is then incorporated into a commercial product or
# service or otherwise used for any commercial purpose, or distribution of the
# source code not in conformity with the restrictions set forth above, whether in
# whole or in part and whether in original or modified form, and any such
# commercial usage is not permitted.
#
# Except as expressly provided for herein, nothing in this License grants to You
# any right, title or interest in and to the intellectual property of Regeneron
# (either expressly or by implication or estoppel).  Notwithstanding anything
# else in this License, nothing contained herein shall limit or compromise the
# rights of Regeneron with respect to its own intellectual property or limit
# its freedom to practice and to develop its products and product candidates.
#
# If the source code, whole or in part and in original or modified form, is
# reproduced, shared or distributed in any manner, it must (1) identify Regeneron
# Pharmaceuticals, Inc. as the original creator, and (2) include the terms of
# this License.
#
# UNLESS OTHERWISE SEPARATELY AGREED UPON, THE SOURCE CODE IS PROVIDED ON AN
# AS-IS BASIS, AND REGENERON PHARMACEUTICALS, INC. MAKES NO REPRESENTATIONS OR
# WARRANTIES OF ANY KIND CONCERNING THE SOURCE CODE, IN WHOLE OR IN PART AND IN
# ORIGINAL OR MODIFIED FORM, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHER
# REPRESENTATIONS OR WARRANTIES. THIS INCLUDES, WITHOUT LIMITATION, WARRANTIES
# OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT,
# ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY, OR THE PRESENCE OR ABSENCE OF
# ERRORS, WHETHER OR NOT KNOWN OR DISCOVERABLE.
#
# In no case shall Regeneron be liable for any loss, claim, damage, or expenses,
# of any kind, which may arise from or in connection with this License or the
# use of the source code. You shall indemnify and hold Regeneron and its
# employees harmless from any loss, claim, damage, expenses, or liability, of
# any kind, from a third-party which may arise from or in connection with this
# License or Your use of the source code.
#
# You agree that this License and its terms are governed by the laws of the
# State of New York, without regard to choice of law rules or the United
# Nations Convention on the International Sale of Goods.
#
# Please reach out to Regeneron Pharmaceuticals Inc./Administrator relating to
# any non-academic or commercial use of the source code.
#
library(data.table)
library(Hmisc)
library(survival)
library(optparse)
library(PGxUtil)
library(rms)
# This script has a strong assumption that we are working on cox models.
#
# Helper functions --------------------------------------------------------
.d <- `[`

fh_concordance <-
  function(full_predict,
           null_predict,
           pheno,
           method = 1) {
    if (!is.vector(full_predict)) {
      full_predict <- as.vector(unlist(full_predict))
    }
    if (!is.vector(null_predict)) {
      null_predict <- as.vector(unlist(null_predict))
    }
    # Our predicted score is related to hazard, not survival, but concordance
    # index was for survival
    fh_test <-
      rcorrp.cens(
        x1 =  1 - null_predict,
        x2 = 1 - full_predict,
        S = pheno,
        method = method
      )
    full_concordance <- fh_test[["x2 more concordant"]]
    full_c <- fh_test[["C X2"]]
    null_concordance <- fh_test[['x1 more concordant']]
    null_c <- fh_test[["C X1"]]
    p_value <- 2 * pnorm(fh_test[['Dxy']] / fh_test[['S.D.']])
    full_c_index <- rcorr.cens(x = 1 - full_predict, S = pheno)
    null_c_index <- rcorr.cens(x = 1 - null_predict, S = pheno)
    return(
      data.table(
        full = full_concordance,
        null = null_concordance,
        p = p_value,
        full_c = full_c_index[["C Index"]],
        full_c_se = full_c_index[["S.D."]],
        null_c = null_c_index[["C Index"]],
        null_c_se = null_c_index[["S.D."]]
      )
    )
  }

frank_nri <- function(x1, x2, y) {
  res <- improveProb(x1, x2, y)
  nri_type <- c("nri", "nri.ev", "nri.ne")
  res_dt <- NULL
  for (i in nri_type) {
    cur_res <-
      data.table(nri = res[[i]],
                 se = res[[paste0("se.", i)]],
                 nri_type = i)
    res_dt <- rbind(res_dt, cur_res)
  }
  zpci <- function(m, se, conf.int) {
    z <- qnorm((1 + conf.int) / 2)
    list(
      z = m / se,
      p = 2 * pnorm(-abs(m / se)),
      lci = m - z * se,
      uci = m + z * se
    )
  }
  res_dt[nri_type == "nri.ev", nri_type := "event"]
  res_dt[nri_type == "nri.ne", nri_type := "nonevent"]
  res_dt[nri_type == "nri", nri_type := "total"]
  res_dt[, c("z", "p", "LCI", "UCI") := zpci(nri, se, 0.95), by = .I]
  idi_res <-
    as.data.table(res[c("idi", "se.idi", "z.idi", "improveSpec", "improveSens")]) |>
    setnames(c("se.idi", "z.idi"), c("se", "z")) |>
    .d(, p := 2 * pnorm(-abs(res$z.idi)))
  return(list(nri = res_dt, idi = idi_res))
}
calculate_nri <-
  function(dat, old_quant, new_quant, pheno) {
    num_event <- sum(dat[, get(pheno)] == 1)
    num_nonevent <- nrow(dat) - num_event
    event_correct <-
      nrow(dat[get(pheno) == 1 &
                 get(new_quant) == get(pheno) &
                 get(old_quant) != get(pheno)])
    event_incorrect <-
      nrow(dat[get(pheno) == 1 &
                 get(new_quant) != get(pheno) &
                 get(old_quant) == get(pheno)])
    nonevent_correct <-
      nrow(dat[get(pheno) == 0 &
                 get(new_quant) == get(pheno) &
                 get(old_quant) != get(pheno)])
    nonevent_incorrect <-
      nrow(dat[get(pheno) == 0 &
                 get(new_quant) != get(pheno) &
                 get(old_quant) == get(pheno)])
    event_nri <- (event_correct - event_incorrect) / num_event
    nonevent_nri <-
      (nonevent_correct - nonevent_incorrect) / num_nonevent
    total_nri <- event_nri + nonevent_nri
    return(list(
      event = event_nri,
      nonevent = nonevent_nri,
      total = total_nri
    ))
  }

apply_new_model_first <-
  function(coef_input,
           protein_name = NULL,
           new_dat,
           pheno,
           time) {
    coef <- copy(coef_input)[!variable %in% "(Intercept)"]
    if (!is.null(protein_name)) {
      coef[variable == "ProteinScore", variable := protein_name]
    }
    # This handling of factors are way more robust compared to what we did before
    coef[!is.na(levels), levels := paste0(variable, levels)]
    coef[is.na(levels), levels := variable]
    coef[covariate == TRUE, Estimate := 0]
    coef_vec <- coef[, Estimate]
    names(coef_vec) <- coef[, levels]
    formula <-
      PGxUtil::make_formula(lhs = paste0("Surv(", time, ", ", pheno, ")"),
                            rhs = coef[, variable])
    # We need to be very careful as coxph uses the init without mapping them
    # to the coefficient, and the order of the coefficients might get shifted
    # which cause problem to the final model. Safetest way will be to "dry run"
    # the coxph once, to get the correct ordering, then use the ordering to
    # get the final model performance
    model_order <-
      coxph(formula,
            data = new_dat,
            iter.max = 0)
    return(list(
      coef = coef_vec,
      ordering = names(model_order$coefficients),
      formula = formula
    ))
  }

apply_new_model <-
  function(coef_input,
           protein_name = NULL,
           new_dat,
           pheno,
           time) {
    tmp <-
      apply_new_model_first(coef_input,
                            protein_name,
                            new_dat,
                            pheno,
                            time)
    model_res <-
      coxph(
        tmp$formula,
        data = new_dat,
        iter.max = 0,
        init = tmp$coef[tmp$ordering]
      )
    return(model_res)
  }

# GND code from Nancy -----------------------------------------------------
kmdec = function(dec.num, dec.name, datain, adm.cens) {
  stopped = 0
  data.sub = datain[datain[, dec.name] == dec.num, ]
  if (sum(data.sub$out) > 1) {
    avsurv = survfit(Surv(tvar, out) ~ 1,
                     data = datain[datain[, dec.name] == dec.num, ], error = "g")
    avsurv.est = ifelse(min(avsurv$time) <= adm.cens,
                        avsurv$surv[avsurv$time ==
                                      max(avsurv$time[avsurv$time <= adm.cens])],
                        1)

    avsurv.stderr = ifelse(min(avsurv$time) <= adm.cens,
                           avsurv$std.err[avsurv$time ==
                                            max(avsurv$time[avsurv$time <= adm.cens])],
                           0)
    avsurv.stderr = avsurv.stderr * avsurv.est

    avsurv.num = ifelse(min(avsurv$time) <= adm.cens,
                        avsurv$n.risk[avsurv$time ==
                                        max(avsurv$time[avsurv$time <= adm.cens])],
                        0)
  } else {
    return(c(0, 0, 0, 0, stopped = -1))
  }
  if (sum(data.sub$out) < 5)
    stopped = 1
  c(avsurv.est, avsurv.stderr, avsurv.num, dec.num, stopped)
}#kmdec

GND.calib = function(pred, tvar, out, cens.t, groups, adm.cens) {
  tvar.t <- ifelse(tvar > adm.cens, adm.cens, tvar)
  out.t <- ifelse(tvar > adm.cens, 0, out)
  groups <- as.numeric(groups)
  datause = data.frame(
    pred = pred,
    tvar = tvar.t,
    out = out.t,
    count = 1,
    cens.t = cens.t,
    dec = groups
  )
  numcat = length(unique(datause$dec))
  groups = sort(unique(datause$dec))
  kmtab = matrix(
    lapply(groups, kmdec, "dec", datain = datause, adm.cens) |>
      unlist(),
    ncol = 5,
    byrow = TRUE
  )

  if (any(kmtab[, 5] == -1)) {
    return(list(
      df = -1,
      chi2gw = -1,
      pvalgw = -1
    ))
  }
  hltab = data.frame(
    group = kmtab[, 4],
    totaln = tapply(datause$count, datause$dec, sum),
    censn = tapply(datause$cens.t, datause$dec, sum),
    numevents = tapply(datause$out, datause$dec, sum),
    expected = tapply(datause$pred, datause$dec, sum),
    kmperc = 1 - kmtab[, 1],
    kmvar = kmtab[, 2] ^ 2,
    kmnrisk = kmtab[, 3],
    expectedperc = tapply(datause$pred, datause$dec, mean)
  )

  hltab$kmnum = hltab$kmperc * hltab$totaln
  hltab$GND_component = ifelse(hltab$kmvar == 0,
                               0,
                               (hltab$kmperc - hltab$expectedperc) ^ 2 / (hltab$kmvar))
  list(
    df = numcat - 1,
    chi2gw = sum(hltab$GND_component),
    pvalgw = 1 - pchisq(sum(hltab$GND_component), numcat - 1)
  )
}#GND.calib

# automated functions -----------------------------------------------------

gather_fh <-
  function(input,
           time,
           pheno,
           full,
           grs_null,
           protein_null) {
    pheno_for_fh <- Surv(input[, get(time)], input[, get(pheno)])
    # From here: https://stats.stackexchange.com/a/117344
    # Reason we need to do 1 - c most likely because:
    # higher predictions mean shorter survival time so the
    # linear predictor is negatively correlated with survival time
    grs_fh <-
      fh_concordance(full_predict = input[, get(full)],
                     null_predict = input[, get(grs_null)],
                     pheno = pheno_for_fh)
    protein_fh <-
      fh_concordance(full_predict = input[, get(full)],
                     null_predict = input[, get(protein_null)],
                     pheno = pheno_for_fh)
    fh_res <- rbind(grs_fh, protein_fh)
    fh_res[, c("type", "fh_type") := list(c("grs", "protein"), "diff")]
    grs_fh <-
      fh_concordance(
        full_predict = input[, get(full)],
        null_predict =  input[, get(grs_null)],
        pheno = pheno_for_fh,
        method = 2
      )
    protein_fh <-
      fh_concordance(
        full_predict = input[, get(full)],
        null_predict = input[, get(protein_null)],
        pheno = pheno_for_fh,
        method = 2
      )
    fh_res_con <- rbind(grs_fh, protein_fh)
    fh_res_con[, c("type", "fh_type") := list(c("grs", "protein"), "con")]
    fh_res <- rbind(fh_res, fh_res_con)
    fh_res <-
      dcast(
        fh_res,
        type ~ fh_type,
        value.var = c(
          "full",
          "null",
          "p",
          "full_c",
          "full_c_se",
          "null_c",
          "null_c_se"
        )
      )
    return(fh_res)
  }

gather_ascertainment <-
  function(predicted,
           pheno,
           quantiles,
           risk_year) {
    rate_res <- NULL
    predicted[, groups := NULL]
    for (quant in quantiles) {
      cur_rate <-
        predicted[!is.na(Predicted), groups :=
                    ifelse(get_quantile(x = Predicted, num_quant = quant) == quant,
                           "Risk",
                           "Normal"),
                  by = c("model", "Perm")] |>
        .d(!is.na(Predicted), .(rate = mean(get(pheno))), by = c("groups", "model")) |>
        .d(, Quantile := quant) |>
        dcast(model + Quantile ~ groups, value.var = "rate")
      rate_res <- rbind(rate_res, cur_rate)
    }
    return(rate_res)
  }


gather_lrt <-
  function(input,
           full_model,
           grs_null_model,
           protein_null_model,
           time,
           pheno) {
    full_model_obj <-
      apply_new_model(
        new_dat = input,
        coef_input = full_model$model,
        protein_name = "FullProtein",
        pheno = pheno,
        time = time
      )
    grs_null_obj <- apply_new_model(
      new_dat = input,
      coef_input = grs_null_model$model,
      protein_name = "FullProtein",
      pheno = pheno,
      time = time
    )
    grs_lrt <- anova(full_model_obj, grs_null_obj)
    protein_null_obj <- apply_new_model(
      new_dat = input,
      coef_input = protein_null_model$model,
      protein_name = "FullProtein",
      pheno = pheno,
      time = time
    )
    protein_lrt <- anova(full_model_obj, protein_null_obj)
    lrt_res <-
      data.table(
        type = c("grs", "protein"),
        ChiSq = c(grs_lrt$Chisq[2], protein_lrt$Chisq[2]),
        LRT_P = c(grs_lrt$`Pr(>|Chi|)`[2], protein_lrt$`Pr(>|Chi|)`[2])
      )
    return(lrt_res)
  }

gather_extra_calibrate <- function(x, y) {
  cur_res <- val.prob(p = x, y = y)
  res <-
    data.table(
      rms_brier = cur_res[["Brier"]],
      Spiegelhalter = cur_res[["S:p"]],
      intercept = cur_res[["Intercept"]],
      slope = cur_res[["Slope"]]
    )
  return(res)
}

refit_basehazard <-
  function(coef_input,
           new_dat,
           pheno,
           time) {
    coef <- copy(coef_input)[!variable %in% "(Intercept)"]
    # This handling of factors are way more robust compared to what we did before
    coef[!is.na(levels), levels := paste0(variable, levels)]
    coef[is.na(levels), levels := variable]
    coef[covariate == TRUE, Estimate := 0]
    coef_vec <- coef[, Estimate]
    names(coef_vec) <- coef[, levels]
    formula <-
      PGxUtil::make_formula(
        lhs = paste0("Surv(", time, ",", pheno, ")"),
        rhs = coef[, variable],
        string_only = TRUE
      )
    # We need to be very careful as coxph uses the init without mapping them
    # to the coefficient, and the order of the coefficients might get shifted
    # which cause problem to the final model. Safetest way will be to "dry run"
    # the coxph once, to get the correct ordering, then use the ordering to
    # get the final model performance
    model_order <-
      coxph(
        as.formula(formula),
        data = new_dat,
        iter.max = 0
      )
    model_res <-
      coxph(
        as.formula(formula),
        data = new_dat,
        iter.max = 0,
        init = coef_vec[names(model_order$coefficients)]
      )

    intercept <-
      data.table::data.table(
        variable = "(Intercept)",
        covariate = FALSE,
        Estimate = -sum(model_res$coefficients * model_res$mean)
      )
    return(list(basehazard = as.data.table(basehaz(model_res)),
                intercept = intercept))
  }

recalibrate_base <-
  function(input, models, pheno, time, quantiles, risk_year) {
    cur_predicted <- NULL
    cur_dat <- copy(input)
    for (i in seq_along(models)) {
      cur_model <- models[[i]]
      if (!is.null(cur_model$proteins)) {
        cur_dat[, ProteinScore := PGxUtil::calculate_predicted_scores(
          input = .SD,
          coefficients = cur_model$proteins,
          transform = PGxUtil::scale0
        )]
      }
      refitted_info <-
        refit_basehazard(
          coef_input = cur_model$model,
          new_dat = cur_dat,
          pheno = pheno,
          time = time
        )
      cur_coef <-
        copy(cur_model$model[!variable %like% "Intercept"]) |>
        rbind(refitted_info$intercept, fill = TRUE)
      cur_dat[, Predicted := PGxUtil::calculate_predicted_scores(
        input = .SD,
        coefficients = copy(cur_coef[covariate == FALSE]),
        basehazard = refitted_info$basehazard,
        risk_year = risk_year,
        probability = TRUE
      )]
      cur_dat[, model := names(models)[i]]
      cur_predicted <-
        rbind(cur_predicted, cur_dat[, c("IID", pheno, time, "model", "Predicted"), with = FALSE])
    }
    cur_predicted[, Perm := "placeholder"]
    refitted_res <-
      gather_calibration(
        predicted = cur_predicted,
        pheno = pheno,
        time = time,
        quantiles = quantiles,
        risk_year = risk_year
      )
    refitted_res[, Perm := NULL]
    return(refitted_res)
  }

gather_calibration <-
  function(predicted,
           pheno,
           time,
           quantiles,
           risk_year) {
    #cur_dat <- data.table::copy(input)
    gnd_res <- NULL
    brier_res <-
      predicted[, gather_extra_calibrate(x = Predicted, y = get(pheno)), by = c("model", "Perm")]
    if (!is.null(quantiles)) {
      for (quant in quantiles) {
        predicted[!is.na(Predicted), Quantiles := PGxUtil::get_quantile(x = Predicted, num_quant = quant), by = c("model", "Perm")]
        cur_gnd <-  predicted[!is.na(Predicted), GND.calib(
          pred = Predicted,
          tvar = get(time),
          out = get(pheno),
          cens.t = risk_year,
          groups = Quantiles,
          adm.cens = risk_year
        ), by = c("model", "Perm")] |>
          .d(, Quantile := quant)
        gnd_res <- rbind(gnd_res, cur_gnd)
      }
      gnd_res[pvalgw == -1, c("pvalgw", "chi2gw") := NA]
      gnd_res[, df := NULL]
      setnames(gnd_res,
               c("pvalgw", "chi2gw"),
               c("gnd_pvalue", "gnd_chi2"))
    }
    calibration_res <- brier_res
    if (!is.null(gnd_res)) {
      calibration_res <-
        merge(brier_res, gnd_res, by = c("model", "Perm"))
    }
    return(calibration_res)
  }

gather_nri <- function(input, full, grs_null, protein_null, pheno) {
  grs_nri <-
    frank_nri(x2  = input[, get(full)],
              x1 = input[, get(grs_null)],
              y = input[, get(pheno)])
  protein_nri <-
    frank_nri(x2  = input[, get(full)],
              x1 = input[, get(protein_null)],
              y = input[, get(pheno)])
  nri_res <-
    rbind(grs_nri$nri[, type := "grs"], protein_nri$nri[, type := "protein"])
  nri_res <-
    dcast(nri_res,
          type ~ nri_type,
          value.var = c("nri", "se", "z", "p", "LCI", "UCI"))
  idi_res <-
    rbind(grs_nri$idi[, type := "grs"], protein_nri$idi[, type := "protein"]) |>
    setnames(c("z", "se", "p"), c("idi_z", "idi_se", "idi_p"))
  return(merge(nri_res, idi_res))
}

gather_hazard_ratio <- function(input, time, pheno, model) {
  dat <- copy(input) |>
    setnames("FullProtein", "ProteinScore")
  hr_res <- coxph(PGxUtil::make_formula(
    lhs = paste0("Surv(", time, ", ", pheno, ")"),
    rhs = c(model$independent, model$covariate)
  ),
  data = dat) |>
    PGxUtil::extract_glm_statistics(c("GRS", "ProteinScore")) |>
    .d(, c("levels", "SE") := NULL) |>
    setnames(c("variable", "Estimate", "pvalue"),
             c("type", "HR", "hr_pvalue")) |>
    .d(, HR := exp(HR)) |>
    .d(, type := gsub("score", "", tolower(type)))
  return(hr_res)
}

gather_or <-
  function(predicted,
           model_list,
           pheno) {
    or_res <- predicted[, {
      covariates <- model_list[[model]]$covariates
      res <- glm(
        formula = PGxUtil::make_formula(
          lhs = pheno,
          rhs = c("scale(Predicted)", covariates)
        ),
        family = binomial,
        data = .SD
      )  |>
        PGxUtil::extract_glm_statistics(coef = "scale(Predicted)") |>
        .d(, OR.UCI := exp(Estimate + 1.96 * SE)) |>
        .d(, OR.LCI := exp(Estimate - 1.96 * SE)) |>
        .d(, Estimate := exp(Estimate)) |>
        setnames(c("Estimate", "pvalue"),
                 c("OR", "OR.Pvalue")) |>
        .d(, c("OR", "OR.Pvalue", "OR.UCI", "OR.LCI"))
    }, by = c("model", "Perm")]
    return(or_res)
  }
gather_all_statistics <-
  function(input,
           time,
           pheno,
           family,
           full_model_name,
           protein_null_model_name,
           grs_null_model_name,
           model_list,
           risk_year = NULL,
           transform,
           quantiles,
           predicted) {
    fh_res <- hr_res <- lrt_res <- NULL
    calibration_res <- NULL
    res <- NULL

    relevant <-
      predicted[model %in% c(
        full_model_name,
        grs_null_model_name,
        protein_null_model_name,
        "Clinical+GRS",
        "Clinical"
      )] |>
      dcast(as.formula(paste0("IID + ", pheno, " + ", time, " ~ model")), value.var = "Predicted")
    if (family == "cox") {
      fh_res <-
        gather_fh(
          input = relevant,
          time = time,
          pheno = pheno,
          full = full_model_name,
          grs_null = grs_null_model_name,
          protein_null = protein_null_model_name
        )
      # Likelihood ratio --------------------------------------------------------
      lrt_res <-
        gather_lrt(
          input = input,
          time = time,
          pheno = pheno,
          full_model = model_list[[full_model_name]],
          grs_null_model = model_list[[grs_null_model_name]],
          protein_null_model = model_list[[protein_null_model_name]]
        )
      res <-  merge(lrt_res, fh_res)
      # Calculate HR in validation ----------------------------------------------
      hr_res <-
        gather_hazard_ratio(
          input = input,
          time = time,
          pheno = pheno,
          model = model_list[[full_model_name]]
        )
      res <- merge(res, hr_res)

      # calibration with brier ------------------------------------------------------------
      if (!is.null(risk_year)) {
        calibration_res <-
          gather_calibration(
            predicted = predicted,
            pheno = pheno,
            time = time,
            quantiles = quantiles,
            risk_year = risk_year
          )
        recalibrate_res <-
          recalibrate_base(
            input = input,
            models = model_list,
            time = time,
            pheno = pheno,
            quantiles = quantiles,
	    risk_year = risk_year
          )
      }
    }

    # NRI analyses ------------------------------------------------------------
    # Don't like this, but will still do it anyway
    nri_res <-
      gather_nri(
        input = relevant,
        full = full_model_name,
        grs_null = grs_null_model_name,
        protein_null = protein_null_model_name,
        pheno = pheno
      )

    # Combine statistic results -----------------------------------------------
    if (is.null(res)) {
      res <- nri_res
    } else{
      res <- merge(res, nri_res)
    }

    # Also want to know the ascertainment --------------------------------------
    if (!is.null(quantiles)) {
      ascertain_res <- gather_ascertainment(
        predicted = predicted,
        pheno = pheno,
        quantiles = quantiles,
        risk_year = risk_year
      )
    }



    # Do logistic regression --------------------------------------------------
    or_res <-
      gather_or(predicted = predicted,
                model_list = model_list,
                pheno = pheno)

    # AUC results -------------------------------------------------------------
    auc_res <-
      predicted[model %in% c("Clinical+GRS+Proteins",
                             "Clinical+GRS",
                             "Clinical",
                             "Proteins"),
                .(auc = pROC::roc(response = get(pheno), predictor = Predicted)$auc),
                by = model]
    return(
      list(
        stat = res,
        calibrate = calibration_res,
        recalibrate = recalibrate_res,
        auc =  auc_res,
        or = or_res,
        ascertain = ascertain_res
      )
    )
  }
# parameters --------------------------------------------------------------

option_list <-
  list(
    make_option(
      c("--family"),
      type = "character",
      default = NULL,
      help = "Data family. Support cox, binomial, binary, logistic, continuous and gaussian"
    ),
    make_option(c("--time"),
                type = "character",
                help = "Column name of the time to event information"),
    make_option(
      c("--input"),
      type = "character",
      help  = "File containing the phenotype and other variables for the validate samples",
      default = NULL
    ),
    make_option(
      c("--models"),
      type = "character",
      help  = "File containing the models",
      default = NULL
    ),
    make_option(
      c("--predicted"),
      type = "character",
      help  = "File containing the predicted values",
      default = NULL
    ),
    make_option(
      c("--pheno"),
      type = "character",
      help = "Column name containing the phenotype of interest.",
      default = NULL
    ),
    make_option(
      c("--protein-cov"),
      type = "character",
      dest = "proteinCov",
      help  = "File containing the protein covariate information",
      default = NULL
    ),
    make_option(
      c("--covariates"),
      type = "character",
      help = "comma separated column names for the covariate columns",
      default = NULL
    ),
    make_option(
      c("--prefix"),
      type = "character",
      help = "Output prefix",
      default = "PGx"
    ),
    make_option(
      c("--secondary"),
      action = "store_true",
      help = "If set, will skip cor test and HR",
      default = FALSE
    ),
    make_option(
      c("--transform"),
      type = "character",
      help = "Transformation to be performed on the protein score. This is to ensure consistency in interpretation of the coefficients",
      default = "rint"
    ),
    make_option(
      c("--quantile"),
      type = "character",
      help  = "comma separated threshold to define the top X quantiles",
      default = NULL
    ),
    make_option(c("--split"),
                type = "character",
                help = "Name of current split"),
    make_option(
      c("--risk-year"),
      type = "numeric",
      dest = "risk",
      help = "risk year for calibration purpose"
    ),
    make_option(c("--sensitivity"),
                type = "character",
                help = "Comma separated list of left censoring to be done")
  )
argv <- parse_args(OptionParser(option_list = option_list))

# Parse input parameter  --------------------------------------------------
perm_name <-  PGxUtil::strsplit0(argv$split, split = "\\.")[5]
quantiles <- NULL
if (!is.null(argv$quantile)) {
  quantiles <-
    PGxUtil::strsplit0(argv$quantile, split = ",") |> as.numeric()
}
transform <- PGxUtil::trans_func(argv$transform)
sample_dat <- data.table::fread(argv$input, header = TRUE)

if (argv$family == "cox" && is.null(argv$time)) {
  argv$time <- "FlUp"
}
if (!is.null(argv$time) && !argv$time %in% colnames(sample_dat)) {
  stop(paste0("Time to event column `", argv$time, "` not found in input"))
}
# This is valid because we have standardized the input
model_list <- list()
for (i in PGxUtil::strsplit0(argv$models, split = ",")) {
  model_list <- append(model_list, readRDS(i))
}
predicted <- NULL
for (i in PGxUtil::strsplit0(argv$predicted, split = ",")) {
  predicted <- rbind(predicted, fread(i), fill = TRUE)
}
ascertainment <- sample_dat[, mean(get(argv$pheno))]
# Predicted is already censored
sample_dat[, c("ProteinOnly", "FullProtein") := list(
  PGxUtil::calculate_predicted_scores(
    input = .SD,
    coefficients = copy(model_list[["Proteins"]]$protein),
    transform = transform
  ),
  PGxUtil::calculate_predicted_scores(
    input = .SD,
    coefficients = copy(model_list[["Clinical+GRS+Proteins"]]$protein),
    transform = transform
  )
)]
full_model_name <- "Clinical+GRS+Proteins"
grs_null_model_name <- "Clinical+Proteins-GRS"
protein_null_model_name <- "Clinical+GRS-Proteins"
predicted[, Perm := perm_name]
protein_names <-
  colnames(sample_dat)[grepl(".*_OID.*", colnames(sample_dat))]
fwrite(
  merge(predicted[, c("IID", "model", "Predicted", "Perm")], sample_dat[, -c(protein_names), with = FALSE], by = "IID"),
  paste0(argv$prefix, "-", perm_name, "-predicted.csv.gz")
)

# Protein Table -----------------------------------------------------------
protein_res <-
  rbind(model_list[["Proteins"]]$protein,
        model_list[[full_model_name]]$protein)[, Perm := perm_name]
fwrite(protein_res,
       paste0(argv$prefix, "-", perm_name, "-protein.csv.gz"))
# correlation result ------------------------------------------------------
# Still need this for correlation analysis
relevant_predicted <-
  predicted[model %in% c(
    full_model_name,
    grs_null_model_name,
    protein_null_model_name,
    "Clinical+GRS",
    "Clinical"
  )] |>
  dcast(as.formula(paste0(
    "IID + ", argv$pheno, " + ", argv$time, " ~ model"
  )), value.var = "Predicted")

cor_input <-
  merge(relevant_predicted[, -c("Clinical+GRS-Proteins",
                                "Clinical+Proteins-GRS",
                                argv$time,
                                argv$pheno), with = FALSE],
        sample_dat[, c("IID", "ProteinOnly", "FullProtein", "GRS")], by = "IID")
cor_input <-
  melt(
    cor_input,
    id.vars = c("IID", "ProteinOnly", "FullProtein"),
    measure.vars = colnames(cor_input) %not% c("IID", "FullProtein")
  )
# Try to get correlation between the protein only score, the proteinScore,
# and the full model scores
cor_r <- cor_input[, lapply(.SD, function(x, y) {
  test <- cor.test(x, y)
  test$estimate
}, value), by = variable, .SDcols = c("ProteinOnly", "FullProtein")] |>
  setnames("variable", "model") |>
  .d(, stat := "r") |>
  melt(id.vars = c("model", "stat"))

cor_p <- cor_input[, lapply(.SD, function(x, y) {
  test <- cor.test(x, y)
  test$p.value
}, value), by = variable, .SDcols = c("ProteinOnly", "FullProtein")] |>
  setnames("variable", "model") |>
  .d(, stat := "p") |>
  melt(id.vars = c("model", "stat"))

cor_res <- rbind(cor_r, cor_p) |>
  dcast(variable ~ model + stat, value.var = "value") |>
  .d(, Perm := perm_name)
fwrite(cor_res,
       paste0(argv$prefix, "-", perm_name, "-cor.csv.gz"))


# gather all statistics ---------------------------------------------------

stat_res <- gather_all_statistics(
  input = sample_dat,
  predicted = predicted,
  time = argv$time,
  pheno = argv$pheno,
  family = argv$family,
  full_model_name = full_model_name,
  protein_null_model_name = protein_null_model_name,
  grs_null_model_name = grs_null_model_name,
  model_list = model_list,
  risk_year = argv$risk,
  transform = transform,
  quantiles = quantiles
)
calibrate <- stat_res$calibrate
recalibrate <- stat_res$recalibrate
stat <- stat_res$stat
or <- stat_res$or
auc <- stat_res$auc |>
  .d(, Perm := perm_name) |>
  dcast(Perm ~ model, value.var = "auc")
ascertain <- stat_res$ascertain
ascertain[, Perm := perm_name]
fwrite(ascertain,
       paste0(argv$prefix, "-", perm_name, "-ascertain.csv.gz"))


if (!is.null(argv$sensitivity)) {
  sensitivity <-
    PGxUtil::strsplit0(argv$sensitivity, split = ",") |> as.numeric()
  control <- which(sample_dat[, get(argv$pheno)] == 0)
  cases <- which(sample_dat[, get(argv$pheno)] == 1)
  num_cases <- length(cases)
  # First count number of samples
  total_sample <- nrow(sample_dat)
  for (year in sensitivity) {
    remove_samples <-
      sample_dat[, get(argv$pheno)] == 1 &
      sample_dat[, get(argv$time)] < year
    n_sample_remove <- sum(remove_samples)
    remain_cases <- num_cases - n_sample_remove
    if (n_sample_remove > 0 && remain_cases > 0) {
      # Don't bother if sample reduction is 0
      down_sample_id <-
        c(control, sample(cases, size = remain_cases))
      down_sample <- sample_dat[down_sample_id,]
      down_sample_pred <- predicted[IID %in% down_sample[, IID]]
      down_sample_res <- gather_all_statistics(
        input = down_sample,
        predicted = down_sample_pred,
        time = argv$time,
        pheno = argv$pheno,
        family = argv$family,
        full_model_name = full_model_name,
        protein_null_model_name = protein_null_model_name,
        grs_null_model_name = grs_null_model_name,
        model_list = model_list,
        risk_year = argv$risk,
        transform = transform,
        quantiles = quantiles
      )
      cur_calibrate <- down_sample_res$calibrate
      if (!is.null(cur_calibrate)) {
        cur_calibrate[,
                      c("Time", "Analysis", "Removed") := list(year, "DownSample", n_sample_remove)]
        calibrate <- rbind(calibrate, cur_calibrate, fill = TRUE)
        cur_recalibrate <- down_sample_res$recalibrate[,
                                                       c("Time", "Analysis", "Removed") := list(year, "DownSample", n_sample_remove)]
        recalibrate <-
          rbind(recalibrate, cur_recalibrate, fill = TRUE)
      }
      cur_stat <-
        down_sample_res$stat |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "DownSample", n_sample_remove))
      cur_auc <- down_sample_res$auc |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "DownSample", n_sample_remove)) |>
        .d(, Perm := perm_name) |>
        dcast(Perm + Time + Analysis + Removed ~ model, value.var = "auc")
      cur_or <- down_sample_res$or |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "DownSample", n_sample_remove))

      stat <- rbind(stat, cur_stat, fill = TRUE)
      auc <- rbind(auc, cur_auc, fill = TRUE)
      or <- rbind(or, cur_or, fill = TRUE)

      # censor
      censored_sample <- sample_dat[remove_samples == FALSE]
      censored_pred <- predicted[IID %in% censored_sample[, IID]]
      censored_res <- gather_all_statistics(
        input = censored_sample,
        predicted = censored_pred,
        time = argv$time,
        pheno = argv$pheno,
        family = argv$family,
        full_model_name = full_model_name,
        protein_null_model_name = protein_null_model_name,
        grs_null_model_name = grs_null_model_name,
        model_list = model_list,
        risk_year = argv$risk,
        transform = transform,
        quantiles = quantiles
      )
      cur_calibrate <- censored_res$calibrate
      if (!is.null(cur_calibrate)) {
        cur_calibrate[,
                      c("Time", "Analysis", "Removed") := list(year, "Censor", n_sample_remove)]
        calibrate <- rbind(calibrate, cur_calibrate, fill = TRUE)
        cur_recalibrate <- censored_res$recalibrate[,
                                                    c("Time", "Analysis", "Removed") := list(year, "Censor", n_sample_remove)]
        recalibrate <-
          rbind(recalibrate, cur_recalibrate, fill = TRUE)
      }
      cur_stat <-
        censored_res$stat |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "Censor", n_sample_remove))
      cur_or <-
        censored_res$or |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "Censor", n_sample_remove))
      cur_auc <- censored_res$auc |>
        .d(,
           c("Time", "Analysis", "Removed") := list(year, "Censor", n_sample_remove)) |>
        .d(, Perm := perm_name) |>
        dcast(Perm + Time + Analysis + Removed ~ model, value.var = "auc")
      stat <- rbind(stat, cur_stat, fill = TRUE)
      or <- rbind(or, cur_or, fill = TRUE)
      auc <- rbind(auc, cur_auc, fill = TRUE)
    }
  }
}
if (!is.null(calibrate)) {
  fwrite(calibrate[, Perm := perm_name],
         paste0(argv$prefix, "-", perm_name, "-calibrate.csv.gz"))
  fwrite(recalibrate[, Perm := perm_name],
         paste0(argv$prefix, "-", perm_name, "-recalibrate.csv.gz"))
}
stat[, Perm := perm_name]
fwrite(stat, paste0(argv$prefix, "-", perm_name, "-stat.csv.gz"))
or[, Perm := perm_name]
fwrite(or, paste0(argv$prefix, "-", perm_name, "-or.csv.gz"))

# AUC results -------------------------------------------------------------

fwrite(auc, paste0(argv$prefix, "-", perm_name, "-auc.csv.gz"))
